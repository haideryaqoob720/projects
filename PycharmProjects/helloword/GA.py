import pandas as pd
import numpy as np
import math

def cal_pop_fitness(equation_inputs, pop, actual_values):
    fitness = np.empty((pop.shape[0], 0))
    print("Fintness :",len(fitness))
    predi_fitness = []

    for chromo in range(pop.shape[0]):
        predi_fitness_lst = np.sum(pop[chromo] *equation_inputs, axis=1)
        for i in range(actual_values.shape[0]):
            predi_fitness.append((predi_fitness_lst[i] - actual_values[i]) ** 2)
        fitness = np.append(fitness, sum(predi_fitness))
    print('type of fitness is : ', fitness)
    return fitness

def select_mating_pool(pop, fitness, num_parents):
    # Selecting the best individuals in the current generation as parents for producing the offspring of the next generation.
    parents = np.empty((num_parents, pop.shape[1]))
    for parent_num in range(num_parents):
        max_fitness_idx = np.where(fitness == np.min(fitness))
        max_fitness_idx = max_fitness_idx[0][0]
        parents[parent_num, :] = pop[max_fitness_idx, :]
        fitness[max_fitness_idx] = float("inf")
    return parents

def crossover (parents, offspring_size):
    offspring = np.empty(offspring_size)
    # The point at which crossover takes place between two parents. Usually, it is at the center.
    crossover_point = np.round(np.uint8(offspring_size[1]/2))

    for k in range(offspring_size[0]):
        # Index of the first parent to mate.
        parent1_idx = k % parents.shape[0]
        # Index of the second parent to mate.
        parent2_idx = (k+1) % parents.shape[0]
        # The new offspring will have its first half of its genes taken from the first parent.
        offspring[k, 0:crossover_point] = parents[parent1_idx, 0:crossover_point]
        # The new offspring will have its second half of its genes taken from the second parent.
        offspring[k, crossover_point:] = parents[parent2_idx, crossover_point:]
    return offspring

def mutation(offspring_crossover, num_mutations=1):
    mutations_counter = np.uint8(offspring_crossover.shape[1] / num_mutations)
    # Mutation changes a number of genes as defined by the num_mutations argument. The changes are random.
    for idx in range(offspring_crossover.shape[0]):
        gene_idx = mutations_counter - 1
        for mutation_num in range(num_mutations):
            # The random value to be added to the gene.
            random_value = np.random.uniform(-1.0, 1.0, 1)
            offspring_crossover[idx, gene_idx] = offspring_crossover[idx, gene_idx] + random_value
            gene_idx = gene_idx + mutations_counter
    return offspring_crossover


dataset = pd.read_csv('/home/haider/Downloads/cancer_reg.csv', sep=',', header=0,
                      usecols=lambda x: x not in ['binnedinc', 'geography', 'target_deathrate'])

act_dataset = pd.read_csv('/home/haider/Downloads/cancer_reg.csv',
                      sep=',', header=0, usecols=['target_deathrate'])


dataset_nan = dataset.fillna(0)

# input of the equation
equation_inputs = dataset_nan.values[:, :]

# no. of weights we are looking to optimize - returns # of rows in equation inputs
num_weights = equation_inputs.shape[1]

sol_per_pop = 8
num_parents_mating = 4

# actual values list
actual_values = np.concatenate(act_dataset.values)
# Defining the population size.
pop_size = (sol_per_pop, num_weights)

# Creating the initial population.
new_population = np.random.uniform(low=-14.0, high=14.0, size=pop_size)

best_outputs = []
num_generations = 100

for generation in range(num_generations):
    print("Generation : ", generation)
    # Measuring the fitness of each chromosome in the population.
    fitness = cal_pop_fitness(equation_inputs, new_population, actual_values)
    best_outputs.append(np.min(fitness))
    # The best result in the current iteration.
    print("Best result : ", np.min(fitness))

    # Selecting the best parents in the population for mating.
    parents = select_mating_pool(new_population, fitness, num_parents_mating)
    print("Parents")
    print(parents)

    # Generating next generation using crossover.
    offspring_crossover = crossover(parents, offspring_size=(pop_size[0] - parents.shape[0], num_weights))
    print((pop_size[0] - parents.shape[0]))
    print("Crossover")
    print(offspring_crossover)

    # Adding some variations to the offspring using mutation.
    offspring_mutation = mutation(offspring_crossover, num_mutations=2)
    print("Mutation")
    print(offspring_mutation)

    # Creating the new population based on the parents and offspring.
    new_population[0:parents.shape[0], :] = parents
    new_population[parents.shape[0]:, :] = offspring_mutation

# Getting the best solution after iterating finishing all generations.
# At first, the fitness is calculated for each solution in the final generation.
fitness = cal_pop_fitness(equation_inputs, new_population, actual_values)

# Then return the index of that solution corresponding to the best fitness.
best_match_idx = np.where(fitness == np.min(fitness))

print("Best solution : ", new_population[best_match_idx, :])
print("Best solution fitness : ", fitness[best_match_idx[0][0]])

print(best_outputs)

import matplotlib.pyplot as plt1

plt1.plot(best_outputs)
plt1.xlabel("Iteration")
plt1.ylabel("Fitness")
plt1.show()