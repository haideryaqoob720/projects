command = ""
started = False
while True != "quit":
    command = input("enter command: ").lower()
    if command == "start":
        if started == True:
            print("car is already started")
        else:
            started = True
            print("car started")
    elif command == "stop":
        if not started:
            print("car is already stop")
        else:
            started = False
            print("car stoped")
    elif command == "help":
        print("""
        to start "start"
        to stop "stop"
        to quit "quit"
        """)
    elif command == 'quit':
        print("game finished")
        break
    else:
        print("it's not a valid command")
        break
