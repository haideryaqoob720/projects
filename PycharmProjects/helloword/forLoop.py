names = ['haider', 'ali', 'ahmed']
for x in names:
    print(x)

for x in range(10):
    print(x)

for x in range(3, 10):
    print(x)

for x in range(4):
    for y in range(3):
        print(f'({x},{y})')

number = [5, 2, 5, 2, 2]
for x_count in number:
    output = ""
    for count in range(x_count):
        output += "*"
    print(output)