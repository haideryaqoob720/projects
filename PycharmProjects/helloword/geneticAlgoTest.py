import numpy as np
import pandas as pd
import math as mt
from math import pow

def cal_pop_fitness(new_population, X,Y):
    # Calculating the fitness value of each solution in the current population.
    # The fitness function calulates the sum of products between each input and its corresponding weight.
    #print("pop")
    #print(pop)
    for i in range(len(new_population)):
        fitness =np.sum(new_population[i] * X, axis=1)
        print("Y prime",fitness)
        for i in range(X.shape[0]):
            err[i] = np.square(fitness[i] - Y[i])
            # print(err[i])
            #err[i] = err[i] * err[i]
        print("Error")
        print(err)
        fiter_list.append( sum(err, len(err)))
        #print("Fitness after Sum",fitness)
        print("fitterlist", fiter_list)
    return fiter_list

def select_mating_pool(pop, fitness, num_parents):
    # Selecting the best individuals in the current generation as parents for producing the offspring of the next generation.
    parents = np.empty((num_parents, pop.shape[1]))
    print("Parents Before Best Selection",parents)
    for parent_num in range(num_parents):
        max_fitness_idx = np.where(fitness == np.min(fitness))
        #print("fitness id",max_fitness_idx).com
        max_fitness_idx = max_fitness_idx[0][0]
        print("Minimum index from fitness",max_fitness_idx)
        #print("fitness id", max_fitness_idx)
        #print("id",max_fitness_idx,"=",pop[max_fitness_idx,:])
        parents[parent_num, :] = pop[max_fitness_idx, :]
        print("After Selection Parents:",parents)
        fitness[max_fitness_idx] = 9999999999999999999999999999999999999999999999999
    return parents

def crossover(parents, offspring_size):
    #print("offsize",offspring_size ) 4,6
    offspring = np.empty(offspring_size)
    # The point at which crossover takes place between two parents. Usually, it is at the center.
    crossover_point = np.round(np.uint8(offspring_size[1]/2))
    for k in range(offspring_size[0]):
        # Index of the first parent to mate.
        parent1_idx = k % parents.shape[0]
        #print("index of parent",parent1_idx)
        # Index of the second parent to mate.
        parent2_idx = (k+1) % parents.shape[0]
        #print("index of parent", parent2_idx)
        # The new offspring will have its first half of its genes taken from the first parent.
        offspring[k, 0:crossover_point] = parents[parent1_idx, 0:crossover_point]
        # The new offspring will have its second half of its genes taken from the second parent.
        offspring[k, crossover_point:] = parents[parent2_idx, crossover_point:]
    return offspring

def mutation(offspring_crossover, num_mutations=1):
    mutations_counter = np.uint8(offspring_crossover.shape[1] / num_mutations)
    #print("mutation counter",offspring_crossover.shape[1])
    # Mutation changes a number of genes as defined by the num_mutations argument. The changes are random.
    for idx in range(offspring_crossover.shape[0]):
        gene_idx = mutations_counter - 1
        #print("gg",gene_idx)
        #print("geneidx",gene_idx)
        for mutation_num in range(num_mutations):
            # The random value to be added to the gene.
            random_value = np.random.uniform(-1.0, 1.0, 1)
            print("random value for mutation",random_value)
            #print(idx,gene_idx)
            offspring_crossover[idx, gene_idx] = offspring_crossover[idx, gene_idx] + random_value
            gene_idx = gene_idx + mutations_counter
    return offspring_crossover

"""
The y=target is to maximize this equation ASAP:
    y = w1x1+w2x2+w3x3+w4x4+w5x5+6wx6
    where (x1,x2,x3,x4,x5,x6)=(4,-2,3.5,5,-11,-4.7)
    What are the best values for the 6 weights w1 to w6?
    We are going to use the genetic algorithm for the best possible values after a number of generations.
"""

# Inputs of the equation.
dataset = pd.read_csv('/home/haider/Downloads/cancer_regChanged.csv', sep=',', header=0)
print(f'DataSet: {dataset}')
#X = dataset.values[:,:5] original
X = dataset.values[:,:] #pick all row and firts 5 columns
#print("X\n",X)
Y = dataset.values[:,2] #pick only 3 column
#print(Y)
err = np.empty(len(X))
X=np.delete(X, [2], axis=1) # delete coulumn 3 which is target_death rate
col=len(X[0])
print("Total Columns:",col)

row=len(X)
print("Range of ROws",range(row))
print("Range of Columns",range(col))
sum_Mean=0
for i in range(row):
    for j in range(col):
        if mt.isnan(X[i,j]):
            # X[i,j] = 999
            for k in range(col):
                if k == j or mt.isnan(X[i,k]):
                    continue
                else:
                    sum_Mean =sum_Mean+ X[i, k]
            sum_Mean=sum_Mean/len(X[0])
            X[i,j]=sum_Mean

print("After Change the NaN to a number",X[0,7])


#################################################################
# acesspoint=X[2,13]
# if  mt.isnan(acesspoint):
#     acesspoint = 999999
# print("Access Point",acesspoint)
#################################################################



#################################################################
# dataf=((dataset-dataset.min())/(dataset.max()-dataset.min()))*28
# new=dataf.values[0:9,0:9]
# # print("DataSet Normalized",len(dataf[0]))
# print("shfgdshjdfzgxcvjk buta",new)
#################################################################
fiter_list=[]
# Number of the weights we are looking to optimize.
num_weights = col

"""
Genetic algorithm parameters:
    Mating pool size
    Population size
"""

sol_per_pop = 8
num_parents_mating = 4

# Defining the population size.
pop_size = (sol_per_pop, num_weights)  # The population will have sol_per_pop chromosome where each chromosome has
# num_weights genes.
# Creating the initial population.

new_population = np.random.uniform(low=-4.0, high=4.0, size=pop_size)
print("New Population",new_population)
print("Actual fitness")


'''
import matplotlib.pyplot as plt
import numpy as np
count, bins, ignored = plt.hist(new_population, 15, normed=True)
plt.plot(bins, np.ones_like(bins), linewidth=2, color='r')
plt.show()
'''
best_outputs = []
num_generations = 100
for generation in range(num_generations):
    print("Generation : ", generation)
    # Measuring the fitness of each chromosome in the population.
    fitness = cal_pop_fitness(new_population, X,Y)
    print("Fitness")
    print(fitness)
    best_outputs.append(np.min(fitness))
    # The best result in the current iteration.
    print("Best result : ", np.min(fitness))

    #print("return all fitness",fitness)
    # Selecting the best parents in the population for mating.
    parents = select_mating_pool(new_population, fitness, num_parents_mating)
    print("Parents")
    print(parents)
    # Generating next generation using crossover.
    offspring_crossover = crossover(parents, offspring_size=(pop_size[0] - parents.shape[0], num_weights))
    print("Crossover")
    print(offspring_crossover)

    # Adding some variations to the offspring using mutation.
    offspring_mutation = mutation(offspring_crossover, num_mutations=2)
    print("Mutation")
    print(offspring_mutation)

    # Creating the new population based on the parents and offspring.
    new_population[0:parents.shape[0], :] = parents
    new_population[parents.shape[0]:, :] = offspring_mutation
    fitness.clear()
# Getting the best solution after iterating finishing all generations.
# At first, the fitness is calculated for each solution in the final generation.
fitness =cal_pop_fitness(new_population, X,Y)
print("Fitness after all generations",fitness)
# Then return the index of that solution corresponding to the best fitness.
best_match_idx = np.where(fitness == np.min(fitness))
best_match_idx=best_match_idx[0][0]
print("Index of min fitest value from fitness",best_match_idx)

print("Best solution : ", new_population[best_match_idx, :])
print("Best solution fitness : ", fitness[best_match_idx])
print("Best Outputs",best_outputs)
import matplotlib.pyplot as plt1

plt1.plot(best_outputs)
plt1.xlabel("Iteration")
plt1.ylabel("Fitness")
plt1.show()