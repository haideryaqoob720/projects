class livingThings:
    def oxygen(self):
        print("living things need oxygen to survive")

class person(livingThings):
    def __init__(self, name):
        self.name = name
    def talk(self):
        print(f"hi talk to {self.name}")


firstObj = person('haider')
firstObj.age = 22
firstObj.talk()
print(f'age of first person is: {firstObj.age}')

secondObj = person("ali")
print(f'name of sencond object is {secondObj.name}')
secondObj.talk()
secondObj.oxygen()