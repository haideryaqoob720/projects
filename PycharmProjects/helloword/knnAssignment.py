import math

f = open('data.txt', 'r');
lines = f.read().splitlines();
f.close();

print(f'all data: {lines}')

features = lines[0].split(', ')[:-1];
print(f'features: {features}')

items = [];

for i in range(1, len(lines)):

    line = lines[i].split(', ');
    print(f'line: {line}')

    itemFeatures = {"Class": line[-1]};
    print(f'item features: {itemFeatures}')
    # Iterate through the features
    for j in range(len(features)):
        f = features[j];
        v = float(line[j]);
        itemFeatures[f] = v;

    items.append(itemFeatures);

print(f'items: {items}')

def distanc(x, y):
    S = 0;
    for key in x.keys():
        S += math.pow(x[key] - y[key], 2);
    return math.sqrt(S);

def UpdateNeighbors(neighbors, item, distance, k):

    if len(neighbors) < k:

        neighbors.append([distance, item['Class']])
        neighbors = sorted(neighbors)
    else:

        print(f'neighbors[-1][0] is : {neighbors[-1][0]}')
        if neighbors[-1][0] > distance:
            neighbors[-1] = [distance, item['Class']]
            neighbors = sorted(neighbors)

    return neighbors

def CalculateNeighborsClass(neighbors, k):
    count = {};
    for i in range(k):
        if (neighbors[i][1] not in count):
            count[neighbors[i][1]] = 1;
        else:
            count[neighbors[i][1]] += 1;
    return count;

def FindMax(countList):
    maximum = -1;
    classification = "";

    for key in countList.keys():
        print(f'key: {key} and countList.Keys: {countList.keys()}')
        if (countList[key] > maximum):
            print(f'countList[key] is this : {countList[key]}')
            maximum = countList[key];
            classification = key;
            print(f'key is {key}')

    return classification, maximum;

def Classify(nItem, k, Items):
    if (k > len(Items)):
        return "k larger than list length";
    neighbors = [];

    for item in Items:
        distance = distanc(nItem, item);
        neighbors = UpdateNeighbors(neighbors, item, distance, k);

        print(f'this is neighbors: {neighbors}')
    count = CalculateNeighborsClass(neighbors, k);
    print(f'count is : {count}')

    return FindMax(count);

newItem = {'Height' : 1.74, 'Weight' : 67, 'Age' : 22};
result = Classify(newItem, 5 , items);
print(f'restult is {result}')



