questions = ["name", "age", 'color', 'game', 'book']
answers = ["My name is Haider", "my age is 21", "I like blue color", "I play hockey", "My best book is Atomic habits "]
enteredQuestion = input("Please ask any question: ").lower()
for q in questions:
    if q in enteredQuestion:
        answerIndex = questions.index(q)
        print(answers[answerIndex])