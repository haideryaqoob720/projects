try:
    computerMarks = int(input("Enter marks of computer: "))
    mathMarks = int(input("Enter marks of math: "))
    physicsMarks = int(input("Enter marks of Physics: "))

    action = [('software engineering'),('mathematics'),('electrical engineering')]
    marksTupple = (computerMarks,mathMarks,physicsMarks)

    def selectSubject():
        maxMarks = 0
        for subjectMarks in marksTupple:
             if maxMarks < subjectMarks:
                maxMarks = subjectMarks
        return marksTupple.index(maxMarks)

    requiredIndex = selectSubject()
    print("You Select " + action[requiredIndex])
except ValueError:
    print('Invalid Value just enter integer value')
