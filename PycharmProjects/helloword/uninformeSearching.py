# Breadth First Search

vertexList = [['Arad', 366], ['Zerind', 374], ['Sibiu', 253], ['Timisors', 329], ['Fagaras', 176], ['Rimicu_Vicea', 193]]
edgeList = [(0, 1), (0, 2), (0, 3), (2, 4), (2, 5)]
graphs = (vertexList, edgeList)


def bfs(graph, start):
    vertexList, edgeList = graph
    visitedList = []
    queue = [start]
    adjacencyList = [[] for vertex in vertexList]

    # fill adjacencyList from graph
    for edge in edgeList:
        adjacencyList[edge[0]].append(edge[1])

    # bfs
    while queue:
        current = queue.pop()
        for neighbor in adjacencyList[current]:
            for completeVertex in vertexList:
                if completeVertex[neighbor]
            if not neighbor in visitedList:
                queue.insert(0, neighbor)
        visitedList.append(current)
    return visitedList

def returnHeurestic(adjacencyList):




print(bfs(graphs, 0))

# Depth First Search

vertexList = [['Arad', 366], ['Zerind', 374], ['Sibiu', 253], ['Timisors', 329], ['Fagaras', 176], ['Rimicu_Vicea', 193]]
edgeList = [(0, 1), (0, 2), (0, 3), (2, 4), (2, 5)]
graphs = (vertexList, edgeList)


def dfs(graph, start):
    vertexList, edgeList = graph
    visitedVertex = []
    stack = [start]
    adjacencyList = [[] for vertex in vertexList]

    for edge in edgeList:
        adjacencyList[edge[0]].append(edge[1])

    while stack:
        current = stack.pop()
        for neighbor in adjacencyList[current]:
            if not neighbor in visitedVertex:
                stack.append(neighbor)
        visitedVertex.append(current)
    return visitedVertex


print(dfs(graphs, 0))