package admin;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddAlerts {

	WebDriver driver;
	
	public void callDriver() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		driver.get("https://admin-test.block360.io/");	
	}
	
	public void setup(String username, String password){
	    driver.findElement(By.id("username")).sendKeys(username);
	    driver.findElement(By.id("password")).sendKeys(password);
	    driver.findElement(By.id("kc-login")).click();
	}
	public void closeAndClearCache() {
	    try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    ResourceBundle.clearCache();
			    driver.quit();
  }
	public void addAlerts() {
	    driver.findElement(By.cssSelector("li:nth-child(11) span")).click();
	    driver.findElement(By.linkText("Alerts Configuration")).click();
	    try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String randomNumber = Integer.toString(getRandomIntegerBetweenRange(200, 300));
	    
	    driver.findElement(By.name("Modal button")).click();
	    driver.findElement(By.name("name")).sendKeys("Danish" + randomNumber);
	    driver.findElement(By.name("email")).sendKeys("Danish"+ randomNumber + "@gmail.com");
	    driver.findElement(By.xpath("//*[@id=\"Phone Prefix\"]/div/div[1]/input")).sendKeys("+92");
	    driver.findElement(By.cssSelector(".vs__search")).sendKeys(Keys.ENTER);
	    String number = Integer.toString(getRandomIntegerBetweenRange(20000, 30000));
	    driver.findElement(By.name("phone")).sendKeys("34569"+number);
	    driver.findElement(By.name("date")).sendKeys("03:03:03");
	    driver.findElement(By.name("date")).sendKeys(Keys.ENTER);

	    driver.findElement(By.name("Update button")).click();
	    
	    try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//	    driver.findElement(By.linkText("launch Edit")).click();
//	    driver.findElement(By.name("Title")).sendKeys("n");
//	    driver.findElement(By.name("date")).sendKeys("8:8:8");
//	    driver.findElement(By.name("date")).sendKeys(Keys.ENTER);
//	    driver.findElement(By.name("Update button")).click();
	    
	    driver.findElement(By.cssSelector(".c .material-icons")).click();
//	    driver.findElement(By.name("name")).sendKeys("Max Musterman");
	    WebElement UpdateName = driver.findElement(By.name("name"));
	    UpdateName.clear();
	    UpdateName.sendKeys("zaryab");
	    String updateNumber = Integer.toString(getRandomIntegerBetweenRange(20000, 30000));
	    WebElement updatedNumber = driver.findElement(By.name("phone"));
	    updatedNumber.clear();
	    updatedNumber.sendKeys("34569"+updateNumber);
	    driver.findElement(By.name("Update button")).click();
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
	public static int getRandomIntegerBetweenRange(int min, int max){
	    int x = (int)(Math.random()*((max-min)+1))+min;
	    return x;
	}
	
}
