package admin;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class AddBank {
	
	WebDriver driver;
	
	public void callDriver() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		driver.get("https://admin-test.block360.io/");	
	}
	
	public void setup(String username, String password){
	    driver.findElement(By.id("username")).sendKeys(username);
	    driver.findElement(By.id("password")).sendKeys(password);
	    driver.findElement(By.id("kc-login")).click();
	}
	  public void closeAndClearCache() {
		    try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				    ResourceBundle.clearCache();
				    driver.quit();
	  }
	
		public void addBank(String BankSymbol, String curr) {
				String selectCurr = "//option[. = '"+curr+" Rupees ("+curr+")']";
//									"//option[. = '  FFF	Rupees (FFF)']"
				driver.findElement(By.cssSelector("li:nth-child(7) span")).click();
			    driver.findElement(By.linkText("Add")).click();
			    driver.findElement(By.id("currency")).click();
			    {
			      WebElement dropdown = driver.findElement(By.id("currency"));
//			      dropdown.findElement(By.xpath("//option[. = 'FFF Rupees (FFF)']")).click();
			      String selectBank = "//option[. = '"+curr+" Rupees ("+curr+")']";
			      dropdown.findElement(By.xpath(selectCurr)).click();
			    }

			    driver.findElement(By.name("Bank Name")).sendKeys(BankSymbol+" Bank");
			    driver.findElement(By.name("Bank Symbol")).sendKeys(BankSymbol);
			    String branchCode = Integer.toString(getRandomIntegerBetweenRange(20000, 30000));
			    driver.findElement(By.name("Branch code")).sendKeys(branchCode);
			    driver.findElement(By.name("Bank First address")).sendKeys("Berlin");
			    driver.findElement(By.name("Bank Second address")).sendKeys("Kaiserdamm 2714057 Berlin");
			    driver.findElement(By.name("Zip")).sendKeys("10115");
			    driver.findElement(By.name("City")).sendKeys("Berlin");
			    driver.findElement(By.cssSelector("#Country .vs__search")).click();
			    driver.findElement(By.cssSelector("#Country .vs__search")).sendKeys("Germany");
			    driver.findElement(By.cssSelector("#Country .vs__search")).sendKeys(Keys.ENTER);
			    driver.findElement(By.name("Phone")).sendKeys("923454445035");
			    driver.findElement(By.name("Swift")).sendKeys("BONUSTS45NX");
			    JavascriptExecutor js1 = (JavascriptExecutor) driver;
			    js1.executeScript("window.scrollBy(0,450)", "");
			    driver.findElement(By.name("Account title")).sendKeys("Danish Saber");
			    driver.findElement(By.name("Iban")).sendKeys("DE36SCBL0000001"+branchCode);
			    driver.findElement(By.cssSelector("#Supported\\ Country .vs__search")).sendKeys("pak");
			    driver.findElement(By.cssSelector("#Supported\\ Country .vs__search")).sendKeys(Keys.ENTER);
			    driver.findElement(By.cssSelector("#Supported\\ Country .vs__search")).sendKeys("Germany");
			    driver.findElement(By.cssSelector("#Supported\\ Country .vs__search")).sendKeys(Keys.ENTER);
			    driver.findElement(By.cssSelector(".col-sm-3:nth-child(3) .custom-control-label")).click();
			    JavascriptExecutor js4 = (JavascriptExecutor) driver;
			    js4.executeScript("window.scrollBy(0,450)", "");
			    try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    driver.findElement(By.cssSelector(".left > .FeeAccounts:nth-child(2) > .custom-control:nth-child(2) > .custom-control-label")).click();
			    driver.findElement(By.name("1 input deposit static fee")).sendKeys("1");
			    driver.findElement(By.cssSelector(".right > .FeeAccounts:nth-child(2) > .custom-control:nth-child(2) > .custom-control-label")).click();
			    driver.findElement(By.name("1 input withdraw static fee")).sendKeys("1");
			    try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    driver.findElement(By.name("Add button")).click();
		    try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public static int getRandomIntegerBetweenRange(int min, int max){
		    int x = (int)(Math.random()*((max-min)+1))+min;
		    return x;
		}
}
//{
//WebElement dropdown = driver.findElement(By.id("currency"));
////dropdown.findElement(By.xpath("//option[. = 'Euro (EUR)']")).click();
//dropdown.findElement(By.xpath(selectCurr)).click();
//}
//{
//WebElement element = driver.findElement(By.id("currency"));
//Actions builder = new Actions(driver);
//builder.moveToElement(element).clickAndHold().perform();
//}
//{
//WebElement element = driver.findElement(By.id("currency"));
//Actions builder = new Actions(driver);
//builder.moveToElement(element).perform();
//}
//{
//WebElement element = driver.findElement(By.id("currency"));
//Actions builder = new Actions(driver);
//builder.moveToElement(element).release().perform();
//}
//driver.findElement(By.id("currency")).click();
