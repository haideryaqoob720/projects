package admin;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddFeeCategory  {
	
	WebDriver driver;
	
	public void callDriver() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		driver.get("https://admin-test.block360.io/");	
	}
	
	public void setup(String username, String password){
	    driver.findElement(By.id("username")).sendKeys(username);
	    driver.findElement(By.id("password")).sendKeys(password);
	    driver.findElement(By.id("kc-login")).click();
	}
	
	  public void addFeeCategory(String testName) {
		    driver.findElement(By.linkText("account_balanceBank Details")).click();
		    driver.findElement(By.linkText("Fee Categories")).click();
		    driver.findElement(By.name("Modal button")).click();
		    driver.findElement(By.name("Fee category name")).sendKeys(testName);
		    driver.findElement(By.name("Fee category description")).sendKeys("testDescription");
		    driver.findElement(By.name("Add button")).click();
		  }
	  public void closeAndClearCache() {
		    try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				    ResourceBundle.clearCache();
				    driver.quit();
	  }
}
