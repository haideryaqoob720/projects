package admin;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddFiatCurrency {
WebDriver driver;
	
	public void callDriver() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		driver.get("https://admin-test.block360.io/");	
	}
	
	public void setup(String username, String password){
	    driver.findElement(By.id("username")).sendKeys(username);
	    driver.findElement(By.id("password")).sendKeys(password);
	    driver.findElement(By.id("kc-login")).click();
	}
	public void closeAndClearCache() {
	    try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    ResourceBundle.clearCache();
			    driver.quit();
  }
	public void addFiatCurr(String curr) {
		driver.findElement(By.cssSelector("li:nth-child(6) span")).click();
	    driver.findElement(By.linkText("Add Fiat")).click();
	    driver.findElement(By.name("Symbol")).sendKeys(curr);
	    driver.findElement(By.name("Name")).sendKeys(curr +" Rupees");
	    driver.findElement(By.name("Priority")).sendKeys("2");
	    driver.findElement(By.name("Min withdrawal")).sendKeys("10");
	    driver.findElement(By.name("Daily withdrawal")).sendKeys("1000");
	    driver.findElement(By.name("Revenue Account")).sendKeys("PK3520211109000001234567");
	    driver.findElement(By.name("Bank name")).sendKeys("Allied Bank");
	    driver.findElement(By.name("Bank symbol")).sendKeys("ABL");
	    String branchCode = Integer.toString(getRandomIntegerBetweenRange(20000, 30000));
	    driver.findElement(By.name("Branch code")).sendKeys(branchCode);
	    driver.findElement(By.name("Swift")).sendKeys("BOFAUS3NXXX");
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("window.scrollBy(0,450)", "");
	    driver.findElement(By.name("First address")).sendKeys("Islamabad");
	    driver.findElement(By.name("Second address")).sendKeys("Islamabad capital territory");
	    driver.findElement(By.name("zip")).sendKeys("44000");
	    driver.findElement(By.name("City")).sendKeys("Islamabad capital territory");
	    driver.findElement(By.name("Phone")).sendKeys("923456994485");
	    driver.findElement(By.xpath("//*[@id=\"Country\"]/div/div[1]/input")).sendKeys("Pakistan");
	    driver.findElement(By.xpath("//*[@id=\"Country\"]/div/div[1]/input")).sendKeys(Keys.ENTER);
	    driver.findElement(By.name("Account title")).sendKeys("Max Mustermann");
	    driver.findElement(By.name("Iban")).sendKeys("PK36SCBL00000011234"+branchCode);
	    JavascriptExecutor js1 = (JavascriptExecutor) driver;
	    js1.executeScript("window.scrollBy(0,350)", "");
	    driver.findElement(By.xpath("//*[@id=\"Supported Countries\"]/div/div[1]/input")).sendKeys("Pakistan");
	    driver.findElement(By.xpath("//*[@id=\"Supported Countries\"]/div/div[1]/input")).sendKeys(Keys.ENTER);
	    driver.findElement(By.xpath("//*[@id=\"Supported Countries\"]/div/div[1]/input")).sendKeys("Ger");
	    driver.findElement(By.xpath("//*[@id=\"Supported Countries\"]/div/div[1]/input")).sendKeys(Keys.ENTER);
	    driver.findElement(By.xpath("//*[@id=\"Supported Countries\"]/div/div[1]/input")).sendKeys("United");
	    driver.findElement(By.xpath("//*[@id=\"Supported Countries\"]/div/div[1]/input")).sendKeys(Keys.ENTER);
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 driver.findElement(By.cssSelector(".col-sm-3:nth-child(3) .custom-control-label")).click();
		 JavascriptExecutor js4 = (JavascriptExecutor) driver;
		    js4.executeScript("window.scrollBy(0,450)", "");
		    try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//		    driver.findElement(By.name("1 input deposit percentage fee")).sendKeys("1");
//		    driver.findElement(By.name("1 input deposit minimum fee")).sendKeys("1");
//		    driver.findElement(By.name("1 input deposit maximum fee")).sendKeys("1");
//		    driver.findElement(By.name("2 input deposit percentage fee")).sendKeys("1");
//		    driver.findElement(By.name("2 input deposit minimum fee")).sendKeys("1");
//		    driver.findElement(By.name("2 input deposit maximum fee")).sendKeys("1");
//		    driver.findElement(By.name("3 input deposit percentage fee")).sendKeys("1");
//		    driver.findElement(By.name("3 input deposit minimum fee")).sendKeys("1");
//		    driver.findElement(By.name("3 input deposit maximum fee")).sendKeys("1");
//		    driver.findElement(By.name("4 input deposit percentage fee")).sendKeys("1");
//		    driver.findElement(By.name("4 input deposit minimum fee")).sendKeys("1");
//		    driver.findElement(By.name("1 input withdraw percentage fee")).sendKeys("1");
//		    driver.findElement(By.name("4 input deposit maximum fee")).sendKeys("1");
//		    driver.findElement(By.name("1 input withdraw percentage fee")).sendKeys("1");
//		    driver.findElement(By.name("1 input withdraw minimum fee")).sendKeys("1");
//		    driver.findElement(By.name("1 input withdraw maximum fee")).sendKeys("1");
//		    driver.findElement(By.name("2 input withdraw percentage fee")).sendKeys("1");
//		    driver.findElement(By.name("2 input withdraw minimum fee")).sendKeys("1");
//		    driver.findElement(By.name("2 input withdraw maximum fee")).sendKeys("1");
//		    driver.findElement(By.name("3 input withdraw percentage fee")).sendKeys("1");
//		    driver.findElement(By.name("3 input withdraw minimum fee")).sendKeys("1");
//		    driver.findElement(By.name("3 input withdraw maximum fee")).sendKeys("1");
//		    driver.findElement(By.name("4 input withdraw percentage fee")).sendKeys("1");
//		    driver.findElement(By.name("4 input withdraw minimum fee")).sendKeys("1");
//		    driver.findElement(By.name("4 input withdraw maximum fee")).sendKeys("1");
		    driver.findElement(By.cssSelector(".left > .FeeAccounts:nth-child(2) > .custom-control:nth-child(2) > .custom-control-label")).click();
		    driver.findElement(By.name("1 input deposit static fee")).sendKeys("1");
		    driver.findElement(By.cssSelector(".right > .FeeAccounts:nth-child(2) > .custom-control:nth-child(2) > .custom-control-label")).click();
		    driver.findElement(By.name("1 input withdraw static fee")).sendKeys("1");
//		    JavascriptExecutor js3 = (JavascriptExecutor) driver;
//		    js3.executeScript("window.scrollBy(0,750)", "");
//		    try {
//						Thread.sleep(2000);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
		    driver.findElement(By.name("Add button")).click();
		    try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 }
	public static int getRandomIntegerBetweenRange(int min, int max){
	    int x = (int)(Math.random()*((max-min)+1))+min;
	    return x;
	}
	 

}
