package admin;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class AddGlobalLimits  {
	
	WebDriver driver;
	
	public void callDriver() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		driver.get("https://admin-test.block360.io/");	
	}
	
	public void setup(String username, String password){
	    driver.findElement(By.id("username")).sendKeys(username);
	    driver.findElement(By.id("password")).sendKeys(password);
	    driver.findElement(By.id("kc-login")).click();
	}
	public void closeAndClearCache() {
	    try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    ResourceBundle.clearCache();
			    driver.quit();
  }
	
	 public void addGlobelLimits() {
		    driver.findElement(By.linkText("settingsConfigurations")).click();
		    WebElement CryptoWithdrawDailyLimits = driver.findElement(By.name("Crypto withdraw daily limits"));
		    CryptoWithdrawDailyLimits.clear();
		    CryptoWithdrawDailyLimits.sendKeys("100");
		    WebElement CryptoWithdrawMonthlyLimits = driver.findElement(By.name("Crypto withdraw monthly limits"));
		    CryptoWithdrawMonthlyLimits.clear();
		    CryptoWithdrawMonthlyLimits.clear();
		    CryptoWithdrawMonthlyLimits.sendKeys("3000");
		    WebElement cryptoWithdrawYearlyLimits = driver.findElement(By.name("Crypto withdraw yearly limits"));
		    cryptoWithdrawYearlyLimits.clear();
		    cryptoWithdrawYearlyLimits.clear();
		    cryptoWithdrawYearlyLimits.sendKeys("36000");
		    WebElement FiatDepositDailyLimits = driver.findElement(By.name("Fiat deposit daily limits"));
		    FiatDepositDailyLimits.clear();
		    FiatDepositDailyLimits.clear();
		    FiatDepositDailyLimits.sendKeys("100");
		    WebElement FiatDepositMonthlyLimits = driver.findElement(By.name("Fiat deposit monthly limits"));
		    FiatDepositMonthlyLimits.clear();
		    FiatDepositMonthlyLimits.clear();
		    FiatDepositMonthlyLimits.sendKeys("3000");
		    WebElement FiatDepositYearlyLimits = driver.findElement(By.name("Fiat deposit yearly limits"));
		    FiatDepositYearlyLimits.clear();
		    FiatDepositYearlyLimits.clear();
		    FiatDepositYearlyLimits.sendKeys("36000");
		    WebElement FiatWithdrawalsDailyLimits = driver.findElement(By.name("Fiat withdrawals daily limits"));
		    FiatWithdrawalsDailyLimits.clear();
		    FiatWithdrawalsDailyLimits.clear();
		    FiatWithdrawalsDailyLimits.sendKeys("101");
		    WebElement FiatWithdrawalsMonthlyLimits = driver.findElement(By.name("Fiat withdrawals monthly limits"));
		    FiatWithdrawalsMonthlyLimits.clear();
		    FiatWithdrawalsMonthlyLimits.clear();
		    FiatWithdrawalsMonthlyLimits.sendKeys("3001");
		    WebElement FiatWithdrawalsYearlyLimits = driver.findElement(By.name("Fiat withdrawals yearly limits"));
		    FiatWithdrawalsYearlyLimits.clear();
		    FiatWithdrawalsYearlyLimits.clear();
		    FiatWithdrawalsYearlyLimits.sendKeys("36001");
		    driver.findElement(By.name("Update button")).click();
		    try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    driver.findElement(By.cssSelector(".btn-ok")).click();
		    try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		    {
		      WebElement dropdown = driver.findElement(By.id("Kyc level"));
		      dropdown.findElement(By.xpath("//option[. = 'Tier 1']")).click();
		    }
		    {
		      WebElement element = driver.findElement(By.id("Kyc level"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(element).clickAndHold().perform();
	    }
		    {
		      WebElement element = driver.findElement(By.id("Kyc level"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).perform();
		    }
		    {
		      WebElement element = driver.findElement(By.id("Kyc level"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).release().perform();
		    }
		    driver.findElement(By.id("Kyc level")).click();
		    driver.findElement(By.cssSelector("#forError > .col-sm-12:nth-child(2)")).click();
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    WebElement CryptoWithdrawDailyLimits1 = driver.findElement(By.name("Crypto withdraw daily limits"));
		    CryptoWithdrawDailyLimits1.clear();
		    CryptoWithdrawDailyLimits1.clear();
		    CryptoWithdrawDailyLimits1.sendKeys("999");
		    WebElement CryptoWithdrawMonthlyLimits1 = driver.findElement(By.name("Crypto withdraw monthly limits"));
		    CryptoWithdrawMonthlyLimits1.clear();
		    CryptoWithdrawMonthlyLimits1.clear();
		    CryptoWithdrawMonthlyLimits1.sendKeys("29970");
		    WebElement cryptoWithdrawYearlyLimits1 = driver.findElement(By.name("Crypto withdraw yearly limits"));
		    cryptoWithdrawYearlyLimits1.clear();
		    cryptoWithdrawYearlyLimits1.clear();
		    cryptoWithdrawYearlyLimits1.sendKeys("359639");
		    WebElement FiatDepositDailyLimits1 = driver.findElement(By.name("Fiat deposit daily limits"));
		    FiatDepositDailyLimits1.clear();
		    FiatDepositDailyLimits1.clear();
		    FiatDepositDailyLimits1.sendKeys("999");
		    WebElement FiatDepositMonthlyLimits1 = driver.findElement(By.name("Fiat deposit monthly limits"));
		    FiatDepositMonthlyLimits1.clear();
		    FiatDepositMonthlyLimits1.clear();
		    FiatDepositMonthlyLimits1.sendKeys("29970");
		    WebElement FiatDepositYearlyLimits1 = driver.findElement(By.name("Fiat deposit yearly limits"));
		    FiatDepositYearlyLimits1.clear();
		    FiatDepositYearlyLimits1.clear();
		    FiatDepositYearlyLimits1.sendKeys("359639");
		    WebElement FiatWithdrawalsDailyLimits1 = driver.findElement(By.name("Fiat withdrawals daily limits"));
		    FiatWithdrawalsDailyLimits1.clear();
		    FiatWithdrawalsDailyLimits1.clear();
		    FiatWithdrawalsDailyLimits1.sendKeys("998");
		    WebElement FiatWithdrawalsMonthlyLimits1 = driver.findElement(By.name("Fiat withdrawals monthly limits"));
		    FiatWithdrawalsMonthlyLimits1.clear();
		    FiatWithdrawalsMonthlyLimits1.clear();
		    FiatWithdrawalsMonthlyLimits1.sendKeys("29969");
		    WebElement FiatWithdrawalsYearlyLimits1 = driver.findElement(By.name("Fiat withdrawals yearly limits"));
		    FiatWithdrawalsYearlyLimits1.clear();
		    FiatWithdrawalsYearlyLimits1.clear();
		    FiatWithdrawalsYearlyLimits1.sendKeys("359639");
		    driver.findElement(By.name("Update button")).click();
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    driver.findElement(By.cssSelector(".btn-ok")).click();
		  }
	 
}
