package admin;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddLegalStatus {
	WebDriver driver;
	
	public void callDriver() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		driver.get("https://admin-test.block360.io/");	
	}
	
	public void setup(String username, String password){
	    driver.findElement(By.id("username")).sendKeys(username);
	    driver.findElement(By.id("password")).sendKeys(password);
	    driver.findElement(By.id("kc-login")).click();
	}
	public void closeAndClearCache() {
	    try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    ResourceBundle.clearCache();
			    driver.quit();
  }
	  public void addLegalstatus() {
		    String randomNumber = Integer.toString(getRandomIntegerBetweenRange(200, 300));
		    driver.findElement(By.cssSelector("li:nth-child(7) span")).click();
		    driver.findElement(By.linkText("Legal Status")).click();
		    driver.findElement(By.name("Modal button")).click();
		    driver.findElement(By.name("Legal status name")).sendKeys("newLegal"+randomNumber);
		    driver.findElement(By.name("Legal status description")).sendKeys("testing");
		    driver.findElement(By.name("Add button")).click();
		    
		   
		  }
		public static int getRandomIntegerBetweenRange(int min, int max){
		    int x = (int)(Math.random()*((max-min)+1))+min;
		    return x;
		}
}
