package admin;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AddMarket {
	WebDriver driver;
	
	public void callDriver() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		driver.get("https://admin-test.block360.io/");	
	}
	
	public void setup(String username, String password){
	    driver.findElement(By.id("username")).sendKeys(username);
	    driver.findElement(By.id("password")).sendKeys(password);
	    driver.findElement(By.id("kc-login")).click();
	}
	public void closeAndClearCache() {
	    try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    ResourceBundle.clearCache();
			    driver.quit();
  }
	public void addMarket(String Base, String Quote) {
		 driver.findElement(By.cssSelector("li:nth-child(5) span:nth-child(2)")).click();	
		 try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		    driver.findElement(By.linkText("Create")).click();
		    String baseCurr = Base+" Rupees ("+Base+")";
		    Select selectingBase = new Select(driver.findElement(By.id("base")));
		    selectingBase.selectByVisibleText(baseCurr);

		    Select fruits = new Select(driver.findElement(By.id("quote")));
		    fruits.selectByVisibleText(Quote);
		    
		    try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		      
		
			 driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/div[2]/div/div/div/div[2]/div[2]/div/div[2]/input[1]")).sendKeys("1");
			 driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/div[2]/div/div/div/div[2]/div[2]/div/div[3]/input[1]")).sendKeys("1");
		    driver.findElement(By.name("min_base")).sendKeys("0.1");
		    driver.findElement(By.name("max_base")).sendKeys("1");
		    driver.findElement(By.name("min_quote")).sendKeys("0.1");
		    driver.findElement(By.name("max_quote")).sendKeys("1");
		    driver.findElement(By.name("tick")).sendKeys("0.1");
		    driver.findElement(By.name("priority")).sendKeys("1");
		    driver.findElement(By.name("Next")).click();
//		    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .custom-control > .custom-control-label")).click();
//		    driver.findElement(By.name("buyer_fee_base")).sendKeys("10");
//		    driver.findElement(By.name("min_buy_base_fee")).sendKeys("0.01");
//		    driver.findElement(By.name("max_buy_base_fee")).sendKeys("0.5");
//		    driver.findElement(By.name("seller_fee_base")).sendKeys("10");
//		    driver.findElement(By.name("min_sell_base_fee")).sendKeys("0.01");
//		    driver.findElement(By.name("max_sell_base_fee")).sendKeys("0.5");
		    driver.findElement(By.cssSelector(".col-sm-6:nth-child(3) > .custom-control > .custom-control-label")).click();
		    driver.findElement(By.name("buyer_fee_quote")).sendKeys("1");
		    driver.findElement(By.name("min_buy_quote_fee")).sendKeys("0.01");
		    driver.findElement(By.name("max_buy_quote_fee")).sendKeys("0.02");
		    driver.findElement(By.name("seller_fee_quote")).sendKeys("1");
		    driver.findElement(By.name("min_sell_quote_fee")).sendKeys("0.01");
		    driver.findElement(By.name("max_sell_quote_fee")).sendKeys("0.02");
		    driver.findElement(By.name("quoteFeeAccount")).sendKeys("0x0424Cf3e9437663E05B07Da13b0272D05fBA5b7E");
		    try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
//		    driver.findElement(By.cssSelector(".sameAsAbove:nth-child(1) > .custom-control-label")).click();
//		    driver.findElement(By.name("mkr_tkr_dis")).sendKeys("15");
		    driver.findElement(By.name("Update")).click();
		    try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	}
	
}
