package admin;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddVolumeBasedDiscount {
WebDriver driver;
	
	public void callDriver() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		driver.get("https://admin-test.block360.io/");	
	}
	
	public void setup(String username, String password){
	    driver.findElement(By.id("username")).sendKeys(username);
	    driver.findElement(By.id("password")).sendKeys(password);
	    driver.findElement(By.id("kc-login")).click();
	}
	public void closeAndClearCache() {
	    try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    ResourceBundle.clearCache();
			    driver.quit();
  }
	 public void addvolumeBasedDiscount() {
		 try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		    driver.findElement(By.cssSelector("li:nth-child(11) span")).click();
		    driver.findElement(By.linkText("Volume Based Discounts")).click();
		    driver.findElement(By.name("Add volume tier")).click();
		    
		    WebElement DiscountLimitsFrom = driver.findElement(By.name("Discount limits from"));
		    DiscountLimitsFrom.clear();
		    DiscountLimitsFrom.sendKeys("1");
		    WebElement DiscountLimitsTo = driver.findElement(By.name("Discount limits to"));
		    DiscountLimitsTo.clear();
		    DiscountLimitsTo.clear();
		    DiscountLimitsTo.sendKeys("99");
		    WebElement discountLimits = driver.findElement(By.name("Discount limits"));
		    discountLimits.clear();
		    discountLimits.clear();
		    discountLimits.sendKeys("10");
		    driver.findElement(By.name("Add volume tier")).click();
//		    driver.findElement(By.cssSelector(".error")).click();
		    WebElement secondTearDiscount = driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-2:nth-child(3) > .form-control"));
		    secondTearDiscount.clear();
		    secondTearDiscount.clear();
		    secondTearDiscount.sendKeys("100");
		    WebElement secondDiscountLimitFrom = driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-2:nth-child(5) > .form-control"));
		    secondDiscountLimitFrom.clear();
		    secondDiscountLimitFrom.clear();
		    secondDiscountLimitFrom.sendKeys("999");
		    WebElement secondDiscountLimitTo = driver.findElement(By.cssSelector(".row:nth-child(2) > .percent > .form-control"));
		    secondDiscountLimitTo.clear();
		    secondDiscountLimitTo.clear();
		    secondDiscountLimitTo.sendKeys("15");
		    driver.findElement(By.name("Add volume tier")).click();
//		    driver.findElement(By.cssSelector(".error")).click();
		    WebElement thirdTearDiscount = driver.findElement(By.cssSelector(".row:nth-child(3) > .col-sm-2:nth-child(3) > .form-control"));
		    thirdTearDiscount.clear();
		    thirdTearDiscount.clear();
		    thirdTearDiscount.sendKeys("1000");
		    WebElement thirdTearDiscountTo = driver.findElement(By.cssSelector(".row:nth-child(3) > .col-sm-2:nth-child(5) > .form-control"));
		    thirdTearDiscountTo.clear();
		    thirdTearDiscountTo.clear();
		    thirdTearDiscountTo.sendKeys("9999");
		    WebElement thirdTearDiscountFrom = driver.findElement(By.cssSelector(".row:nth-child(3) > .percent > .form-control"));
		    thirdTearDiscountFrom.clear();
		    thirdTearDiscountFrom.clear();
		    thirdTearDiscountFrom.sendKeys("20");
		    
//		    driver.findElement(By.name("Discount limits from")).sendKeys("0");
//		    driver.findElement(By.name("Discount limits to")).sendKeys("99");
//		    driver.findElement(By.name("Discount limits")).sendKeys("10");
//		    driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-2:nth-child(3) > .form-control")).sendKeys("100");
//		    driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-2:nth-child(5) > .form-control")).sendKeys("999");
//		    driver.findElement(By.cssSelector(".row:nth-child(2) > .percent > .form-control")).sendKeys("15");
//		    driver.findElement(By.cssSelector(".row:nth-child(3) > .col-sm-2:nth-child(3) > .form-control")).sendKeys("1000");
//		    driver.findElement(By.cssSelector(".row:nth-child(3) > .col-sm-2:nth-child(5) > .form-control")).sendKeys("9999");
//		    driver.findElement(By.cssSelector(".row:nth-child(3) > .percent > .form-control")).sendKeys("20");
		    driver.findElement(By.name("Update")).click();
		  }

}
