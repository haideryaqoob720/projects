package admin;

import java.util.Scanner;

public class IntegratingTestCases {
	

	public static void main(String[] args) {
		CreateCredentialsFile fileClass = new CreateCredentialsFile();
		String[] credentials;
		credentials = fileClass.creatingFile();
		String adminGmail = credentials[0];
		String adminPass = credentials[1];
		String currencyCreated = "";
		String bankForAccount = "";
		String TokenMarket = "";
		String quoteRevenueAddress = "";
		char task ;
		do {
			System.out.println("***********************************************************************************************************************************\n");
			System.out.println("Please Add Character In Upper Case\n");
			System.out.println("Press F for creating fiat currency then bank then Account then token and finally Market\n");
			System.out.println("Press G to create new Global Limits, Volume Based Discount and Alerts AND Legal Status.\n If going to press G. Then Remember to delete previous volume based discount\n");
			System.out.println("Press E to exit\n");
			System.out.println("***********************************************************************************************************************************\n");
			Scanner input = new Scanner(System.in);
			task = input.next().charAt(0);
	      switch(task) {
	      case 'F' :
	        	 System.out.println("Please Add 3 Characters Symbol To create new Fiat Currency This will be base Curreny While creating Market");
		    		Scanner input3 = new Scanner(System.in);
		    		String currencySymbol = input3.next();
		    		currencyCreated = currencySymbol;
		    		
		    		   System.out.println("Please Add 3 Characters Symbol To create new bank");
			    		Scanner input1 = new Scanner(System.in);
			    		String bankSymbol = input1.next();
			    		bankForAccount = bankSymbol;
			    		
			    		  System.out.println("Please Add 3 Characters Symbol To create new Account");
				    		Scanner input2 = new Scanner(System.in);
				    		String AccountSymbol = input2.next();
				    		
				    		 System.out.println("Please Add 3 Characters Symbol To create new Token. This will be quote currency while creating Market");
					    		Scanner input98 = new Scanner(System.in);
					    		String TokenSymbol = input98.next();
					    		TokenMarket = TokenSymbol+" Block ("+TokenSymbol+")";
					    		System.out.println("Please Enter Valid Token address To create new Token");
					    		Scanner inpu98 = new Scanner(System.in);
					    		String tokenAddress = inpu98.next();
//					    		quoteRevenueAddress = tokenAddress;
					    		
//					    		System.out.println("Please Enter Valid Revenue address To create new Market");
//					    		Scanner inpu89 = new Scanner(System.in);
//					    		String revenueAddress = inpu89.next();
					    		
					    		
			    		
		            System.out.println("Creating new fiat Currency"); 
		        	AddFiatCurrency addFiatCurr = new AddFiatCurrency();
		    		addFiatCurr.callDriver();
		    		addFiatCurr.setup(adminGmail, adminPass);
		    		addFiatCurr.addFiatCurr(currencySymbol);
		    		addFiatCurr.closeAndClearCache();
	 
	         
	            System.out.println("Creating new bank"); 
	    		AddBank addBank = new AddBank();
	    		addBank.callDriver();
	    		addBank.setup(adminGmail, adminPass);
	    		addBank.addBank(bankSymbol, currencyCreated);
	    		addBank.closeAndClearCache();
	         
	        	
		            System.out.println("Creating new Account"); 
		            addAccount addingAccount = new addAccount();
		    		addingAccount.callDriver();
		    		addingAccount.setup(adminGmail, adminPass);
		    		addingAccount.addAccounts(AccountSymbol, bankForAccount+" Bank", currencyCreated);
		    		addingAccount.closeAndClearCache();

		            System.out.println("Creating new Token"); 
		            AddToken addToken = new AddToken();
		    		addToken.callDriver();
		    		addToken.setup(adminGmail, adminPass);
		    		addToken.addToken(TokenSymbol, tokenAddress);
		    		addToken.closeAndClearCache();

		            System.out.println("Creating new Market");
	    		AddMarket addMarket = new AddMarket();
	    		addMarket.callDriver();
	    		addMarket.setup(adminGmail, adminPass);
	    		addMarket.addMarket(currencyCreated, TokenMarket);
	    		addMarket.closeAndClearCache();
	            break;
	         case 'G' :
		            System.out.println("Adding New Global limits");
		        	AddGlobalLimits addGlobal = new AddGlobalLimits();
		    		addGlobal.callDriver();
		    		addGlobal.setup(adminGmail, adminPass);
		    		addGlobal.addGlobelLimits();
		    		addGlobal.closeAndClearCache();
//		            break;
//	         case 'V' :
		            System.out.println("Adding new volume base discounts");
		    		AddVolumeBasedDiscount addVolumeDiscount = new AddVolumeBasedDiscount();
		    		addVolumeDiscount.callDriver();
		    		addVolumeDiscount.setup(adminGmail, adminPass);
		    		addVolumeDiscount.addvolumeBasedDiscount();
		    		addVolumeDiscount.closeAndClearCache();

		            System.out.println("Creating new Alert"); 
		        	AddAlerts addAlerts = new AddAlerts();
		    		addAlerts.callDriver();
		    		addAlerts.setup(adminGmail, adminPass);
		    		addAlerts.addAlerts();
		    		addAlerts.closeAndClearCache();
		    		
//		    		  System.out.println("Creating new Legal Status"); 
//			    		AddLegalStatus addLegal = new AddLegalStatus();
//			    		addLegal.callDriver();
//			    		addLegal.setup(adminGmail, adminPass);
//			    		addLegal.addLegalstatus();
//			    		addLegal.closeAndClearCache();
		            break;
	         case 'E' :
	        	 System.out.println("You sucessfully Exit");

	         default :
	            System.out.println("Invalid Selection");
	      }
		}while(task != 'E');	
	}

}
