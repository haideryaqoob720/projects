package admin;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class addAccount {
	WebDriver driver;
	
	public void callDriver() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		driver.get("https://admin-test.block360.io/");	
	}
	
	public void setup(String username, String password){
	    driver.findElement(By.id("username")).sendKeys(username);
	    driver.findElement(By.id("password")).sendKeys(password);
	    driver.findElement(By.id("kc-login")).click();
	}
	public void closeAndClearCache() {
	    try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    ResourceBundle.clearCache();
			    driver.quit();
  }
	  public void addAccounts(String accountTitle, String bank, String curr) {
		    driver.findElement(By.cssSelector("li:nth-child(7) span")).click();
		    driver.findElement(By.linkText("Accounts")).click();
		    driver.findElement(By.linkText("Add")).click();
		    driver.findElement(By.id("Currency symbol")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("Currency symbol"));
		      String selectCurr = "//option[. = '"+curr+" Rupees ("+curr+")']";
//		      dropdown.findElement(By.xpath("//option[. = 'PKR (PKR)']")).click();
		      dropdown.findElement(By.xpath(selectCurr)).click();
		    }
		    driver.findElement(By.id("Bank id")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("Bank id"));
		      String selectBank = "//option[. = '"+bank+"']";
//		      dropdown.findElement(By.xpath("//option[. = 'Habib Banks']")).click();
		      dropdown.findElement(By.xpath(selectBank)).click();
		    }
		    driver.findElement(By.name("Account title")).sendKeys(accountTitle);
		    driver.findElement(By.cssSelector(".vs__search")).sendKeys("German");
		    driver.findElement(By.cssSelector(".vs__search")).sendKeys(Keys.ENTER);
		    driver.findElement(By.cssSelector(".vs__search")).sendKeys("Pakis");
		    driver.findElement(By.cssSelector(".vs__search")).sendKeys(Keys.ENTER);
		    String branchCode = Integer.toString(getRandomIntegerBetweenRange(20000, 30000));
		    driver.findElement(By.name("Iban")).sendKeys("DET12876547658"+branchCode);
		    driver.findElement(By.cssSelector(".col-sm-3:nth-child(3) .custom-control-label")).click();
//		    driver.findElement(By.name("1 input deposit percentage fee")).sendKeys("1");
//		    driver.findElement(By.name("1 input deposit minimum fee")).sendKeys("1");
//		    driver.findElement(By.name("1 input deposit maximum fee")).sendKeys("1");
//		    driver.findElement(By.name("1 input withdraw percentage fee")).sendKeys("1");
//		    driver.findElement(By.name("1 input withdraw minimum fee")).sendKeys("1");
//		    driver.findElement(By.name("1 input withdraw maximum fee")).sendKeys("1");
//		    driver.findElement(By.name("2 input deposit percentage fee")).sendKeys("1");
//		    driver.findElement(By.name("2 input deposit minimum fee")).sendKeys("1");
//		    driver.findElement(By.name("2 input deposit maximum fee")).sendKeys("1");
//		    driver.findElement(By.name("2 input withdraw percentage fee")).sendKeys("1");
//		    driver.findElement(By.name("2 input withdraw minimum fee")).sendKeys("1");
//		    driver.findElement(By.name("2 input withdraw maximum fee")).sendKeys("1");
		    
		    driver.findElement(By.cssSelector(".left > .FeeAccounts:nth-child(2) > .custom-control:nth-child(2) > .custom-control-label")).click();
		    driver.findElement(By.cssSelector(".right > .FeeAccounts:nth-child(2) > .custom-control:nth-child(2) > .custom-control-label")).click();
		    driver.findElement(By.name("1 input deposit static fee")).sendKeys("1");
		    driver.findElement(By.name("1 input withdraw static fee")).sendKeys("1");
		    JavascriptExecutor js4 = (JavascriptExecutor) driver;
		    js4.executeScript("window.scrollBy(0,450)", "");
		    try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    driver.findElement(By.name("Add button")).click();
		    try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		  }
		public static int getRandomIntegerBetweenRange(int min, int max){
		    int x = (int)(Math.random()*((max-min)+1))+min;
		    return x;
		}

}
