package util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class TestUtil {
	
	
	static Workbook book;
	static Sheet sheet;
	
	public static String TESTDATA_SHEET_PATH ="/home/haider/Downloads/username.xlsx";
	
	
	public static Object[] getTestData(String sheetName)
	{
		
		FileInputStream file = null;
		try {
			
			file = new FileInputStream(TESTDATA_SHEET_PATH);
		
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			book = WorkbookFactory.create(file);
		}catch(IOException e) {
			e.printStackTrace();
		}
		
		sheet=book.getSheet(sheetName);
		Object[] data = new Object[sheet.getLastRowNum()];
		for(int i = 0;i<sheet.getLastRowNum(); i++) {
			
				data[i]=sheet.getRow(i+1).getCell(i).toString();
					
			
		
		}
		return data;
		
	}

}
