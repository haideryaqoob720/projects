package haider;

	import java.awt.Dimension;
	import java.util.Scanner;
	import java.util.concurrent.TimeUnit;
	import org.openqa.selenium.support.ui.Select;
	//import org.apache.bcel.generic.Select;
	import org.openqa.selenium.By;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.interactions.Actions;

	public class KnowYC {

		WebDriver driver;
		
		public void invokeBrowserTrader() {
			try {
				System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
				driver = new ChromeDriver();
				driver.manage().deleteAllCookies();
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
				driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
				
			driver.get("https://qagcu.block360.io");
//			driver.get("https://admin.gsuexchange.com/");

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public void startKyc(String traderlogin, String traderPass) {
			 // Test name: Untitled
		    // Step # | name | target | value | comment
		    // 1 | open | / |  | 
//		    driver.get("");
		    driver.findElement(By.linkText("Log in")).click();
			driver.findElement(By.id("username")).sendKeys(traderlogin);
			driver.findElement(By.id("password")).sendKeys(traderPass);
		    driver.findElement(By.id("kc-login")).click();
		    try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    driver.findElement(By.xpath("//*[@id=\"navbarSupportedContent\"]/ul[2]/li[1]/a")).click();
		    
		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div/div[2]/button")).click();
		    driver.findElement(By.cssSelector(".btn")).click();
		    driver.findElement(By.cssSelector(".first")).click();
		    // 10 | click | css=.agree:nth-child(1) .custom-control-label |  | 
		    driver.findElement(By.cssSelector(".agree:nth-child(1) .custom-control-label")).click();
		    // 11 | click | css=.agree:nth-child(2) .custom-control-label |  | 
		    driver.findElement(By.cssSelector(".agree:nth-child(2) .custom-control-label")).click();
		    // 12 | click | css=.btn-yellow |  | 
		    driver.findElement(By.cssSelector(".btn-yellow")).click();
		    // 13 | mouseDown | css=.multiselect__select |  | 
		    {
		      WebElement element = driver.findElement(By.cssSelector(".multiselect__select"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).clickAndHold().perform();
		    }
		    // 14 | mouseUp | css=.multiselect__input |  | 
		    {
		      WebElement element = driver.findElement(By.cssSelector(".multiselect__input"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).release().perform();
		    }
		    // 15 | click | css=.multiselect |  | 
		    driver.findElement(By.cssSelector(".multiselect")).click();
		    // 16 | type | css=.multiselect__input | +92 | 
		    driver.findElement(By.cssSelector(".multiselect__input")).sendKeys("+92");
		    // 17 | sendKeys | css=.multiselect__input | ${KEY_ENTER} | 
		    driver.findElement(By.cssSelector(".multiselect__input")).sendKeys(Keys.ENTER);
		    // 18 | click | name=Phone Number |  | 
		    driver.findElement(By.name("Phone Number")).click();
		    // 19 | type | name=Phone Number | 3115349167 | 
		    Scanner input = new Scanner(System.in);
	    	System.out.print("Enter your number ");
	    	String numb = input.next();
	    	System.out.println("entered phone number = " + numb);
		    driver.findElement(By.name("Phone Number")).sendKeys(numb);
		    // 20 | click | css=.btn |  | 
		    driver.findElement(By.cssSelector(".btn")).click();
		    Scanner input2 = new Scanner(System.in);
	    	System.out.print("Enter OTP ");
	    	String pass = input.next();
	    	System.out.println("OTP entered = " + pass);
		    // 21 | type | id=code | 253406 | 
		    driver.findElement(By.id("code")).sendKeys(pass);
		    // 22 | click | css=.btn |  | 
		    driver.findElement(By.cssSelector(".btn")).click();
		    // 23 | click | name=First Name |  | 
		    driver.findElement(By.name("First Name")).click();
		    // 24 | type | name=First Name | Haider | 
		    driver.findElement(By.name("First Name")).sendKeys("Haider");
		    // 25 | type | name=Last Name | Yaqoob | 
		    driver.findElement(By.name("Last Name")).sendKeys("Yaqoob");

		    driver .findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[5]/div/div[2]")).click();
		    driver.findElement(By.name("Country of Birth")).sendKeys("Pakistan");
		    driver.findElement(By.name("Country of Birth")).sendKeys(Keys.ENTER);
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    driver .findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[6]/div[1]")).click();
		    driver.findElement(By.name("Year of Birth")).sendKeys("1998");
		    // 33 | sendKeys | name=Year of Birth | ${KEY_ENTER} | 
		    driver.findElement(By.name("Year of Birth")).sendKeys(Keys.ENTER);
		    driver .findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[6]/div[2]")).click();
		    driver.findElement(By.name("Month of Birth")).sendKeys("03");
		    // 36 | sendKeys | name=Month of Birth | ${KEY_ENTER} | 
		    driver.findElement(By.name("Month of Birth")).sendKeys(Keys.ENTER);
		    driver .findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[6]/div[3]")).click();
		    driver.findElement(By.name("Day of Birth")).sendKeys("23");
		    // 39 | sendKeys | name=Day of Birth | ${KEY_ENTER} | 
		    driver.findElement(By.name("Day of Birth")).sendKeys(Keys.ENTER);
		
		    driver.findElement(By.name("Birth place")).sendKeys("Kamra");
		    driver .findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[8]/div/div[2]")).click();
		    driver.findElement(By.name("Citizenship")).sendKeys("Pak");
		    // 48 | sendKeys | name=Citizenship | ${KEY_ENTER} | 
		    driver.findElement(By.name("Citizenship")).sendKeys(Keys.ENTER);
		    // 49 | click | css=.btn |  | 
		    driver.findElement(By.cssSelector(".btn")).click();
		    // 50 | mouseDown | css=.multiselect--active > .multiselect__tags |  | 
//		    {
//		      WebElement element = driver.findElement(By.cssSelector(".multiselect--active > .multiselect__tags"));
//		      Actions builder = new Actions(driver);
//		      builder.moveToElement(element).clickAndHold().perform();
//		    }
//		    // 51 | mouseUp | name=Country |  | 
//		    {
//		      WebElement element = driver.findElement(By.name("Country"));
//		      Actions builder = new Actions(driver);
//		      builder.moveToElement(element).release().perform();
//		    }
//		    // 52 | click | css=.multiselect--active > .multiselect__tags |  | 
//		    driver.findElement(By.cssSelector(".multiselect--active > .multiselect__tags")).click();
//		    // 53 | type | name=Country | Pak | 
		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[3]/div/div[1]/div/div/div[2]")).click();
		    driver.findElement(By.name("Country")).sendKeys("Pak");
		    // 54 | sendKeys | name=Country | ${KEY_ENTER} | 
		    driver.findElement(By.name("Country")).sendKeys(Keys.ENTER); 
		    driver.findElement(By.name("Street Name")).sendKeys("14");
		    // 57 | click | name=Street Floor |  | 
		    driver.findElement(By.name("Street Floor")).click();
		    // 58 | type | name=Street Floor | 2nd Floor | 
		    driver.findElement(By.name("Street Floor")).sendKeys("2nd Floor");
		    // 59 | click | name=Postal Code |  | 
		    driver.findElement(By.name("Postal Code")).click();
		    // 60 | type | name=Postal Code | 43600 | 
		    driver.findElement(By.name("Postal Code")).sendKeys("43600");
		    // 61 | click | name=City |  | 
		    driver.findElement(By.name("City")).click();
		    // 62 | type | name=City | Attock | 
		    driver.findElement(By.name("City")).sendKeys("Attock");
		    // 63 | click | name=Country ID |  | 
		    driver.findElement(By.name("Country ID")).sendKeys("Pakistan");
		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[3]/button")).click();
		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[4]/button")).click();

//		    // 85 | mouseUp | name=Document Type |  | 
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		  
		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div[1]/div")).click();
		    driver.findElement(By.name("Document Type")).sendKeys("National");
		    driver.findElement(By.name("Document Type")).sendKeys(Keys.ENTER);
//		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div[1]/div/div[2]")).sendKeys("National ID Card");
//		    driver.findElement(By.name("Document Number")).sendKeys("Nat");
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    driver.findElement(By.name("Document Number")).sendKeys(Keys.ENTER);
		    // 89 | type | name=Document Number | 4230148931643 | 
		    driver.findElement(By.name("Document Number")).sendKeys("4230148931643");
		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div[3]/div/div/div[1]/div[2]/span")).click();
		    driver.findElement(By.name("Issue Date")).sendKeys("2014");
		    driver.findElement(By.name("Issue Date")).sendKeys(Keys.ENTER);
		 
		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div[3]/div/div/div[2]/div[2]/span")).click();
		    driver.findElement(By.cssSelector(".multiselect--active .multiselect__input")).sendKeys("04");
		    driver.findElement(By.cssSelector(".multiselect--active .multiselect__input")).sendKeys(Keys.ENTER);
		   
		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div[3]/div/div/div[3]/div[2]/span")).click();
		    driver.findElement(By.cssSelector(".multiselect--active .multiselect__input")).sendKeys("3");
		    driver.findElement(By.cssSelector(".multiselect--active .multiselect__input")).sendKeys(Keys.ENTER);
		    
		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div[4]/div/div/div[1]/div[2]/span")).click();
		    driver.findElement(By.name("Expiration Date")).sendKeys("2025");
		    driver.findElement(By.name("Expiration Date")).sendKeys(Keys.ENTER);
		 
		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div[4]/div/div/div[2]/div[2]/span")).click();
		    driver.findElement(By.cssSelector(".multiselect--active .multiselect__input")).sendKeys("04");
		    driver.findElement(By.cssSelector(".multiselect--active .multiselect__input")).sendKeys(Keys.ENTER);
		   
		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div[4]/div/div/div[3]/div[2]/span")).click();
		    driver.findElement(By.cssSelector(".multiselect--active .multiselect__input")).sendKeys("3");
		    driver.findElement(By.cssSelector(".multiselect--active .multiselect__input")).sendKeys(Keys.ENTER);

		    driver.findElement(By.name("Document Issue Place")).click();
		    // 117 | type | name=Document Issue Place | Nadra | 
		    driver.findElement(By.name("Document Issue Place")).sendKeys("Nadra");
		    Scanner inputj = new Scanner(System.in);
		    System.out.println("Please select pictures from your pc. Now Press any key and then enter.");
	    	String tes = input.next();
		    driver.findElement(By.cssSelector(".btn")).click();
		    // 123 | click | name=IBAN |  | 
		    driver.findElement(By.name("IBAN")).click();
		    // 124 | type | name=IBAN | safddwqfr32q4r3rdfsaf | 
		    driver.findElement(By.name("IBAN")).sendKeys("safddwqfr32q4r3rdfsaf");
		    // 125 | click | name=SWIFT |  | 
		    driver.findElement(By.name("SWIFT")).click();
		    // 126 | type | name=SWIFT | asdfsad | 
		    driver.findElement(By.name("SWIFT")).sendKeys("23442345");
		
		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[1]/div[2]/div/div[3]/div/div[2]/span")).click();
		    driver.findElement(By.name("Currency")).sendKeys("EUR");
		    driver.findElement(By.name("Currency")).sendKeys(Keys.ENTER);
		    driver.findElement(By.cssSelector(".btn-dark")).click();
		    // 135 | click | css=.btn-yellow |  | 
		    driver.findElement(By.cssSelector(".btn-yellow")).click();
		}

	
}
