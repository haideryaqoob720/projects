package haider;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.Select;



//import org.apache.bcel.generic.Select;
//import org.apache.bcel.generic.Select;
//import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class newFile {
	

	WebDriver driver;
	
	public void invokeBrowser() {
		try {
			System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
			driver = new ChromeDriver();
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
			
		driver.get("https://qaadmin.block360.io");
//		driver.get("https://admin.gsuexchange.com/");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void signIn(String username, String Pass) {
		driver.findElement(By.id("username")).sendKeys(username);
		driver.findElement(By.id("password")).sendKeys(Pass);
//		driver.findElement(By.id("username")).sendKeys("billal.ariif@gmail.com");
//		driver.findElement(By.id("password")).sendKeys("Bilal0341");
		driver.findElement(By.id("kc-login")).click();
		
	}
	public void addFiatCurrency(String fi) {
	    driver.findElement(By.cssSelector("li:nth-child(6) span")).click();
	    driver.findElement(By.linkText("Add Fiat")).click();
	    driver.findElement(By.cssSelector(".row:nth-child(1) > .col-sm-6:nth-child(2) > .form-control:nth-child(2)")).sendKeys(fi);
	    driver.findElement(By.cssSelector(".row:nth-child(1) > .col-sm-6:nth-child(3) > .form-control:nth-child(2)")).sendKeys("100");
	    driver.findElement(By.cssSelector(".row:nth-child(1) > .col-sm-6:nth-child(2) > .form-control:nth-child(4)")).sendKeys(fi.toLowerCase() + "curr");
	    driver.findElement(By.cssSelector(".row:nth-child(1) > .col-sm-6:nth-child(3) > .form-control:nth-child(4)")).sendKeys("1000");
	    driver.findElement(By.cssSelector(".row:nth-child(1) > .col-sm-6:nth-child(2) > .form-control:nth-child(6)")).sendKeys("50");
	    driver.findElement(By.cssSelector(".row:nth-child(1) > .col-sm-6:nth-child(3) > .form-control:nth-child(6)")).sendKeys(fi.toLowerCase() +"selinium@gmail.com");
	    driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-6:nth-child(2) > .form-control:nth-child(2)")).sendKeys("Selinium Bank" + fi.toLowerCase());
	    driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-6:nth-child(2) > .form-control:nth-child(4)")).sendKeys(fi +"B");
	    driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-6:nth-child(3) > .form-control:nth-child(2)")).sendKeys(fi +"Atock selinium bank");
	    driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-6:nth-child(3) > .form-control:nth-child(4)")).sendKeys(fi + "Kamra Road, Punjab");
	    driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-6:nth-child(2) > .form-control:nth-child(6)")).sendKeys(fi + "SBEUR4400");
	    driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-6:nth-child(3) > .form-control:nth-child(6)")).sendKeys("34344");
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .form-control:nth-child(8)")).sendKeys(fi +"SBEUR321321");
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(3) > .form-control:nth-child(8)")).sendKeys(fi + "Kamra");
	    driver.findElement(By.cssSelector(".form-control:nth-child(10)")).sendKeys("02343123123");
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[2]/div[3]/div/div/div[1]/input")).sendKeys("Pakistan");
	    driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[2]/div[3]/div/div/div[1]/input")).sendKeys(Keys.ENTER);
	    driver.findElement(By.cssSelector(".row:nth-child(3) > .col-sm-6:nth-child(2) > .form-control")).sendKeys(fi +"X360 Exchange Private Account");
	    driver.findElement(By.cssSelector(".row:nth-child(3) > .col-sm-6:nth-child(3) > .form-control")).sendKeys(fi +"IBANSBEURX3603914131");
	    JavascriptExecutor jse = (JavascriptExecutor)driver;
	    jse.executeScript("scroll(0, 1000);");
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    driver.findElement(By.cssSelector(".col-sm-3:nth-child(2) .custom-control-label")).click();
	    driver.findElement(By.cssSelector(".left > .FeeAccounts:nth-child(2) .form-control")).sendKeys("2"); 
	    driver.findElement(By.cssSelector(".right > .FeeAccounts:nth-child(2) .form-control")).sendKeys("5");
	    driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[6]/div/button[1]")).click();
	  }
	
	
	
	public void market() {
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[1]/ul/li[5]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div/div[9]/button")).click();
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/ul/div/a")).click();
		Select baseCurr = new Select(driver.findElement(By.className("filterSelect")));
		baseCurr.selectByIndex(2);
		Select quoteCurr = new Select(driver.findElement(By.className("filterSelect")));
		quoteCurr.selectByIndex(2);
		
	}
	String toftTokenIs = "";
	String ethTokenIs = "";
	String fiatSymbolIs = "";
	public void currencies() {
		System.out.println("Press 1 for Toft Token: ");
		System.out.println("Press 2 for ETH Token: ");
		System.out.println("Press 3 To Add Fiat Currency: ");
		System.out.println("Press 4 To Add All : ");
		Scanner input = new Scanner(System.in);
		int curr = input.nextInt();
		if (curr == 1) {
			System.out.println("Enter Symbol of Toft Token");
			Scanner tof = new Scanner(System.in);
			String toftSymbol = input.next();
			toftTokenIs = toftSymbol;
			System.out.println("Creating Toft token ");
			addCryptoCurrency(curr, toftSymbol);
		}else if (curr == 2) {
			System.out.println("Enter Symbol of ethereum token ");
			Scanner eth = new Scanner(System.in);
			String ethSymbol = input.next();
			ethTokenIs = ethSymbol;
			System.out.println("Creating Ethereum token ");
			addCryptoCurrency(2, ethSymbol);
		}else if (curr == 3) {
			System.out.println("Enter Symbol For Fiat Currency ");
			Scanner eth = new Scanner(System.in);
			String fiatCurr = input.next();
			fiatSymbolIs = fiatCurr;
			System.out.println("Creating Fiat currency ");
			addFiatCurrency(fiatCurr);
		}else if (curr == 4) {
			
			System.out.println("Enter Symbol of Toft Token");
			Scanner tof = new Scanner(System.in);
			String toftSymbol = input.next();
			toftTokenIs = toftSymbol;
			System.out.println("Creating Toft token ");
			addCryptoCurrency(1, toftSymbol);
			
			System.out.println("Enter Symbol of ethereum token ");
			Scanner eth = new Scanner(System.in);
			String ethSymbol = input.next();
			ethTokenIs = ethSymbol;
			System.out.println("Creating Ethereum token ");
			addCryptoCurrency(2, ethSymbol);
			
			System.out.println("Enter Symbol For Fiat Currency ");
			Scanner ethe = new Scanner(System.in);
			String fiatCurr = input.next();
			fiatSymbolIs = fiatCurr;
			System.out.println("Creating Fiat currency ");
			addFiatCurrency(fiatCurr);
			
		}
		
		
	}
	public void addCryptoCurrency(int curr , String symbol ) {
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[1]/ul/li[6]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/ul/div/a[1]")).click();
		driver.findElement(By.id("symbol")).sendKeys(symbol);
		driver.findElement(By.id("name")).sendKeys(symbol +"token");
		driver.findElement(By.id("decimals")).sendKeys("8");
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[2]/div[2]/input[1]")).sendKeys("5");
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[2]/div[2]/input[2]")).sendKeys("20");
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[2]/div[2]/input[3]")).sendKeys("0.01");
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[2]/div[2]/input[4]")).sendKeys("0.1");
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[2]/div[2]/input[5]")).sendKeys("0x0424Cf3e9437663E05B07Da13b0272D05fBA5b7E");
		
		//#app > div.main > div > div > form > div:nth-child(1) > div:nth-child(3) > select
		String token;
		if(curr == 1) {
			token = "TOFT NETWORK (TOFT)";
		}else
			token = "ETHEREUM (ETH)";
		Select dropdown = new Select(driver.findElement(By.className("filterSelect")));
		dropdown.selectByVisibleText(token);
//		dropdown.selectByIndex(curr);
		driver.findElement(By.id("minwithdraw")).sendKeys("0x766e08fe58c141d977754941ce0c878c4af097fa");
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[1]/div[3]/input[2]")).sendKeys("51");
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[2]/div[3]/input[1]")).sendKeys("100");
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[2]/div[3]/input[2]")).sendKeys("25");
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[2]/div[3]/input[3]")).sendKeys("27000");
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[2]/div[3]/input[4]")).sendKeys("100");
		
		   JavascriptExecutor jse = (JavascriptExecutor)driver;
		    jse.executeScript("scroll(0, 1000);");
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		driver.findElement(By.cssSelector(".btn-primary")).click();	
	}

	public void updateFiat() {
		 // 3 | click | css=li:nth-child(6) span |  | 
	    driver.findElement(By.cssSelector("li:nth-child(6) span")).click();
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/table/tbody/tr[1]/td[7]/a")).click();
	    // 4 | click | css=tr:nth-child(2) .material-icons |  | 
//	    driver.findElement(By.cssSelector("tr:nth-child(2) .material-icons")).click();
//	    // 5 | click | css=.row:nth-child(1) > .col-sm-6:nth-child(3) > .form-control:nth-child(2) |  | 
//	    driver.findElement(By.cssSelector(".row:nth-child(1) > .col-sm-6:nth-child(3) > .form-control:nth-child(2)")).click();
//	    // 6 | close |  |  | 
//	    driver.close();
//	    // 7 | type | css=.row:nth-child(1) > .col-sm-6:nth-child(3) > .form-control:nth-child(2) | 5 | 
	    driver.findElement(By.cssSelector(".row:nth-child(1) > .col-sm-6:nth-child(3) > .form-control:nth-child(2)")).sendKeys("6");
	    // 8 | click | css=.btn-primary |  | 
	    driver.findElement(By.cssSelector(".btn-primary")).click();
	  
	}
	public void updateCrypto() {
		   driver.findElement(By.cssSelector("li:nth-child(6) span")).click();
		    try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    driver.findElement(By.xpath("//*[@id=\"tab1\"]/div/div/table/tbody/tr[2]/td[7]/a")).click();
//		  // 9 | click | css=tr:nth-child(1) .material-icons |  | 
//	    driver.findElement(By.cssSelector("tr:nth-child(1) .material-icons")).click();
//	    // 10 | click | css=.col-sm-6:nth-child(2) > .form-control:nth-child(6) |  | 
//	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .form-control:nth-child(6)")).click();
//	    // 11 | type | css=.col-sm-6:nth-child(2) > .form-control:nth-child(6) | 3 | 
	    driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[1]/div[3]/input[1]")).sendKeys("1");
	    // 12 | click | css=.btn-primary |  | 
	    JavascriptExecutor jse = (JavascriptExecutor)driver;
	    jse.executeScript("scroll(0, 1000);");
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    driver.findElement(By.cssSelector(".btn-primary")).click();
	}
	public void makingMarkets() {
	    for(int i=1;i<=10;i++){  
	        System.out.println(i);  
	    } 
	    String intArray[]; 
	}
	public void marketCreat() {
		 driver.findElement(By.cssSelector("li:nth-child(5) span")).click();
		    // 4 | click | linkText=Create |  | 
		    driver.findElement(By.linkText("Create")).click();
//		    #app > div.main > div > div > form > div > div.col-sm-12.basicConfig > div > div:nth-child(1) > select
		  //*[@id="app"]/div[2]/div/div/form/div/div[2]/div/div[1]/select
		    String token = "TOFT NETWORK (TOFT)";
		    String token1 = "EEITOKEN (EEI)";
//			if(curr == 1) {
//				token = "TOFT NETWORK (TOFT)";
//			}else
//				token = "ETHEREUM (ETH)";
			Select dropdown = new Select(driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div/div[2]/div/div[1]/select")));
			dropdown.selectByVisibleText(token);
//	    {
//	      WebElement dropdown = driver.findElement(By.cssSelector(".col-sm-6:nth-child(1) > select"));
//	      dropdown.findElement(By.xpath("//option[. = 'TOFT NETWORK (TOFT)']")).click();
//	    }
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//	    Select dropdown2 = new Select(driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > select")));
//	    ((WebDriver) dropdown2).findElement(By.xpath("//option[. = 'ETHEREUM (ETH)']")).click();
////	    {
////		      WebElement dropdown2 =(WebElement) new Select (driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > select")));
////		      dropdown2.findElement(By.xpath("//option[. = 'ETHEREUM (ETH)']")).click();
////		    }
	    Select dropdown2 = new Select(driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > select")));
		dropdown2.selectByIndex(8);
//	    Select dropdow = new Select(driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div/div[2]/div/div[1]/select")));
//		dropdow.selectByVisibleText(token1);
	    
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(1) > .form-control:nth-child(4)")).sendKeys("1");
	    // 17 | click | css=.col-sm-6:nth-child(2) > .form-control:nth-child(4) |  | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .form-control:nth-child(4)")).click();
	    // 18 | type | css=.col-sm-6:nth-child(2) > .form-control:nth-child(4) | 200 | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .form-control:nth-child(4)")).sendKeys("200");
	    // 19 | click | css=.col-sm-6:nth-child(1) > .form-control:nth-child(6) |  | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(1) > .form-control:nth-child(6)")).click();
	    // 20 | type | css=.col-sm-6:nth-child(1) > .form-control:nth-child(6) | 0.01 | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(1) > .form-control:nth-child(6)")).sendKeys("0.01");
	    // 21 | click | css=.col-sm-6:nth-child(2) > .form-control:nth-child(6) |  | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .form-control:nth-child(6)")).click();
	    // 22 | type | css=.col-sm-6:nth-child(2) > .form-control:nth-child(6) | 0.000001 | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .form-control:nth-child(6)")).sendKeys("0.000001");
	    // 23 | click | css=.col-sm-6:nth-child(1) > .form-control:nth-child(6) |  | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(1) > .form-control:nth-child(6)")).click();
	    // 24 | click | css=.col-sm-6:nth-child(1) > .form-control:nth-child(8) |  | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(1) > .form-control:nth-child(8)")).click();
	    // 25 | type | css=.col-sm-6:nth-child(1) > .form-control:nth-child(8) | 1000 | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(1) > .form-control:nth-child(8)")).sendKeys("1000");
	    // 26 | click | css=.col-sm-6:nth-child(2) > .form-control:nth-child(8) |  | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .form-control:nth-child(8)")).click();
	    // 27 | type | css=.col-sm-6:nth-child(2) > .form-control:nth-child(8) | 100 | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .form-control:nth-child(8)")).sendKeys("100");
	    // 28 | click | css=.col-sm-6:nth-child(1) > .form-control:nth-child(10) |  | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(1) > .form-control:nth-child(10)")).click();
	    // 29 | type | css=.col-sm-6:nth-child(1) > .form-control:nth-child(10) | 0.0001 | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(1) > .form-control:nth-child(10)")).sendKeys("0.0001");
	    // 30 | click | css=.col-sm-6:nth-child(2) > .form-control:nth-child(10) |  | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .form-control:nth-child(10)")).click();
	    // 31 | type | css=.col-sm-6:nth-child(2) > .form-control:nth-child(10) | 53 | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .form-control:nth-child(10)")).sendKeys("53");
	 
	    JavascriptExecutor jse = (JavascriptExecutor)driver;
	    jse.executeScript("scroll(0, 1000);");
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    // 32 | click | css=.row:nth-child(1) > .col-sm-1 .custom-control-label |  | 
	    driver.findElement(By.cssSelector(".row:nth-child(1) > .col-sm-1 .custom-control-label")).click();
	    // 33 | click | css=.row:nth-child(2) > .percent > .form-control |  | 
	    driver.findElement(By.cssSelector(".row:nth-child(2) > .percent > .form-control")).click();
	    // 34 | type | css=.row:nth-child(2) > .percent > .form-control | 1 | 
	    driver.findElement(By.cssSelector(".row:nth-child(2) > .percent > .form-control")).sendKeys("1");
	    // 35 | click | css=.row:nth-child(2) > .col-sm-4:nth-child(2) > .form-control |  | 
	    driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-4:nth-child(2) > .form-control")).click();
	    // 36 | type | css=.row:nth-child(2) > .col-sm-4:nth-child(2) > .form-control | 0.00000001 | 
	    driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-4:nth-child(2) > .form-control")).sendKeys("0.00000001");
	    // 37 | click | css=.row:nth-child(2) > .col-sm-4:nth-child(3) > .form-control |  | 
	    driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-4:nth-child(3) > .form-control")).click();
	    // 38 | type | css=.row:nth-child(2) > .col-sm-4:nth-child(3) > .form-control | 1 | 
	    JavascriptExecutor jseo = (JavascriptExecutor)driver;
	    jse.executeScript("scroll(0, 1000);");
	    driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-4:nth-child(3) > .form-control")).sendKeys("1");
	    // 39 | click | css=.row:nth-child(2) > .col-sm-4:nth-child(2) |  | 
	    driver.findElement(By.cssSelector(".row:nth-child(2) > .col-sm-4:nth-child(2)")).click();
	    // 40 | mouseDownAt | css=.row:nth-child(3) > .percent > .form-control | 58,13 | 
	    {
	      WebElement element = driver.findElement(By.cssSelector(".row:nth-child(3) > .percent > .form-control"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(element).clickAndHold().perform();
	    }
	    // 41 | mouseMoveAt | css=.row:nth-child(3) > .percent > .form-control | 58,13 | 
	    {
	      WebElement element = driver.findElement(By.cssSelector(".row:nth-child(3) > .percent > .form-control"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(element).perform();
	    }
	    // 42 | mouseUpAt | css=.row:nth-child(3) > .percent > .form-control | 58,13 | 
	    {
	      WebElement element = driver.findElement(By.cssSelector(".row:nth-child(3) > .percent > .form-control"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(element).release().perform();
	    }
	    // 43 | click | css=.row:nth-child(3) > .percent > .form-control |  | 
	    driver.findElement(By.cssSelector(".row:nth-child(3) > .percent > .form-control")).click();
	    // 44 | type | css=.row:nth-child(3) > .percent > .form-control | 1 | 
	    driver.findElement(By.cssSelector(".row:nth-child(3) > .percent > .form-control")).sendKeys("1");
	    // 45 | click | css=.row:nth-child(3) > .col-sm-4:nth-child(2) > .form-control |  | 
	    driver.findElement(By.cssSelector(".row:nth-child(3) > .col-sm-4:nth-child(2) > .form-control")).click();
	    // 46 | type | css=.row:nth-child(3) > .col-sm-4:nth-child(2) > .form-control | 0.00000001 | 
	    driver.findElement(By.cssSelector(".row:nth-child(3) > .col-sm-4:nth-child(2) > .form-control")).sendKeys("0.00000001");
	    // 47 | click | css=.row:nth-child(3) > .col-sm-4:nth-child(3) > .form-control |  | 
	    driver.findElement(By.cssSelector(".row:nth-child(3) > .col-sm-4:nth-child(3) > .form-control")).click();
	    // 48 | type | css=.row:nth-child(3) > .col-sm-4:nth-child(3) > .form-control | 1 | 
	    driver.findElement(By.cssSelector(".row:nth-child(3) > .col-sm-4:nth-child(3) > .form-control")).sendKeys("1");
	    // 49 | click | css=.col-sm-6:nth-child(2) > .form-control:nth-child(2) |  | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .form-control:nth-child(2)")).click();
	    // 50 | type | css=.col-sm-6:nth-child(2) > .form-control:nth-child(2) | 0xCf78bBf76545B7134FcceD0D11b69c17e2923b5c | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .form-control:nth-child(2)")).sendKeys("0xCf78bBf76545B7134FcceD0D11b69c17e2923b5c");
	    // 51 | click | css=.col-sm-6:nth-child(1) > .form-control:nth-child(2) |  | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(1) > .form-control:nth-child(2)")).click();
	    // 52 | type | css=.col-sm-6:nth-child(1) > .form-control:nth-child(2) | 0xCf78bBf76545B7134FcceD0D11b69c17e2923b5c | 
	    driver.findElement(By.cssSelector(".col-sm-6:nth-child(1) > .form-control:nth-child(2)")).sendKeys("0xCf78bBf76545B7134FcceD0D11b69c17e2923b5c");
	    // 53 | click | css=.btn |  | 
	    driver.findElement(By.cssSelector(".btn")).click();
	}
	

	public static void main(String[] args) {
		//Making object of LoginFile
		file fileClass = new file();
		String[] credentials;
		credentials = fileClass.creatingFile();
		String adminGmail = credentials[0];
		String adminPass = credentials[1];
		String traderGmail = credentials[2];
		String traderPass = credentials[3];
		
		newFile confObj = new newFile();
		confObj.invokeBrowser();
		confObj.signIn(adminGmail, adminPass);
		confObj.currencies();
		
//		Making object of kyc
		KnowYC kyc = new KnowYC();
		kyc.invokeBrowserTrader();
		kyc.startKyc(traderGmail, traderPass);
		
		
		
//		confObj.addFiatCurrency("new");
//		confObj.addCryptoCurrency();
//		confObj.updateFiat();
//		try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		confObj.updateCrypto();
//		confObj.marketCreat();
	}

}
