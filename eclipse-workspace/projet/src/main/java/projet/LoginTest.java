package projet;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import util.TestUtil;



public class LoginTest {
	
	 WebDriver driver;
	
	@BeforeMethod
	public void setUp()
	{
		
	    System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver =new ChromeDriver();
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		
		driver.get("https://opensource-demo.orangehrmlive.com");
		
	}
	
	
	@DataProvider
	public Object[][] getLoginData()
	{
		Object data[][]=TestUtil.getTestData("Sheet1");
		return data;
	}
	
	
	
	@Test(dataProvider="getLoginData")
	public void loginTest(String username,String password) {
		
		driver.findElement(By.id("txtUsername")).sendKeys("username");
		driver.findElement(By.id("txtPassword")).sendKeys("password");
		
		WebElement loginBtn = driver.findElement(By.id("btnLogin"));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", loginBtn);
		
		
		
	}
	
	
	
	
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
