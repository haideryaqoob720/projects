package TestNGData;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import junit.framework.Assert;
import util.TestUtil;

public class addFiatCurrency {
	WebDriver driver;
	
	@BeforeTest
	public void callDriver() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.get("https://admin-test.block360.io/");	
	}
	
	@DataProvider
	public Object[][] getLogin() {
		Object data [][]= TestUtil.getTestData("login");
		return data;
	}
	
	
	@Test(dataProvider = "getLogin")
	public void setup(String username, String password){
		driver.findElement(By.linkText("Log in")).click();
		driver.findElement(By.id("username")).sendKeys("username");
		driver.findElement(By.id("password")).sendKeys("password");
	    driver.findElement(By.id("kc-login")).click();

	}
	
	
	
	@AfterMethod
	void closeDriver() {
		driver.quit();
	}
}
