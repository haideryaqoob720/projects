package restAssuredApiAutomation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class addFiatCurrency {

	WebDriver driver;
	
	
	@BeforeTest
	public void callDriver() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		driver.get("https://admin-test.block360.io/");	
	}
	
	@BeforeMethod
	public void setup(){
	    driver.findElement(By.id("username")).sendKeys("billal.ariif@gmail.com");
	    driver.findElement(By.id("password")).sendKeys("x360x360");
	    driver.findElement(By.id("kc-login")).click();
	    
	}
//	@Test(priority = 1)
//	  public void addGlobelLimits() {
//	    driver.findElement(By.linkText("settingsConfigurations")).click();
//	    WebElement CryptoWithdrawDailyLimits = driver.findElement(By.name("Crypto withdraw daily limits"));
//	    CryptoWithdrawDailyLimits.clear();
//	    CryptoWithdrawDailyLimits.sendKeys("100");
//	    WebElement CryptoWithdrawMonthlyLimits = driver.findElement(By.name("Crypto withdraw monthly limits"));
//	    CryptoWithdrawMonthlyLimits.clear();
//	    CryptoWithdrawMonthlyLimits.clear();
//	    CryptoWithdrawMonthlyLimits.sendKeys("3000");
//	    WebElement cryptoWithdrawYearlyLimits = driver.findElement(By.name("Crypto withdraw yearly limits"));
//	    cryptoWithdrawYearlyLimits.clear();
//	    cryptoWithdrawYearlyLimits.clear();
//	    cryptoWithdrawYearlyLimits.sendKeys("36000");
//	    WebElement FiatDepositDailyLimits = driver.findElement(By.name("Fiat deposit daily limits"));
//	    FiatDepositDailyLimits.clear();
//	    FiatDepositDailyLimits.clear();
//	    FiatDepositDailyLimits.sendKeys("100");
//	    WebElement FiatDepositMonthlyLimits = driver.findElement(By.name("Fiat deposit monthly limits"));
//	    FiatDepositMonthlyLimits.clear();
//	    FiatDepositMonthlyLimits.clear();
//	    FiatDepositMonthlyLimits.sendKeys("3000");
//	    WebElement FiatDepositYearlyLimits = driver.findElement(By.name("Fiat deposit yearly limits"));
//	    FiatDepositYearlyLimits.clear();
//	    FiatDepositYearlyLimits.clear();
//	    FiatDepositYearlyLimits.sendKeys("36000");
//	    WebElement FiatWithdrawalsDailyLimits = driver.findElement(By.name("Fiat withdrawals daily limits"));
//	    FiatWithdrawalsDailyLimits.clear();
//	    FiatWithdrawalsDailyLimits.clear();
//	    FiatWithdrawalsDailyLimits.sendKeys("101");
//	    WebElement FiatWithdrawalsMonthlyLimits = driver.findElement(By.name("Fiat withdrawals monthly limits"));
//	    FiatWithdrawalsMonthlyLimits.clear();
//	    FiatWithdrawalsMonthlyLimits.clear();
//	    FiatWithdrawalsMonthlyLimits.sendKeys("3001");
//	    WebElement FiatWithdrawalsYearlyLimits = driver.findElement(By.name("Fiat withdrawals yearly limits"));
//	    FiatWithdrawalsYearlyLimits.clear();
//	    FiatWithdrawalsYearlyLimits.clear();
//	    FiatWithdrawalsYearlyLimits.sendKeys("36001");
//	    driver.findElement(By.name("Update button")).click();
//	    try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	    driver.findElement(By.cssSelector(".btn-ok")).click();
//	    try {
//				Thread.sleep(3000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//	    {
//	      WebElement dropdown = driver.findElement(By.id("Kyc level"));
//	      dropdown.findElement(By.xpath("//option[. = 'Tier 1']")).click();
//	    }
//	    {
//	      WebElement element = driver.findElement(By.id("Kyc level"));
//      Actions builder = new Actions(driver);
//      builder.moveToElement(element).clickAndHold().perform();
//    }
//	    {
//	      WebElement element = driver.findElement(By.id("Kyc level"));
//	      Actions builder = new Actions(driver);
//	      builder.moveToElement(element).perform();
//	    }
//	    {
//	      WebElement element = driver.findElement(By.id("Kyc level"));
//	      Actions builder = new Actions(driver);
//	      builder.moveToElement(element).release().perform();
//	    }
//	    driver.findElement(By.id("Kyc level")).click();
//	    driver.findElement(By.cssSelector("#forError > .col-sm-12:nth-child(2)")).click();
//	    try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//	    
//	    WebElement CryptoWithdrawDailyLimits1 = driver.findElement(By.name("Crypto withdraw daily limits"));
//	    CryptoWithdrawDailyLimits1.clear();
//	    CryptoWithdrawDailyLimits1.clear();
//	    CryptoWithdrawDailyLimits1.sendKeys("999");
//	    WebElement CryptoWithdrawMonthlyLimits1 = driver.findElement(By.name("Crypto withdraw monthly limits"));
//	    CryptoWithdrawMonthlyLimits1.clear();
//	    CryptoWithdrawMonthlyLimits1.clear();
//	    CryptoWithdrawMonthlyLimits1.sendKeys("29970");
//	    WebElement cryptoWithdrawYearlyLimits1 = driver.findElement(By.name("Crypto withdraw yearly limits"));
//	    cryptoWithdrawYearlyLimits1.clear();
//	    cryptoWithdrawYearlyLimits1.clear();
//	    cryptoWithdrawYearlyLimits1.sendKeys("359639");
//	    WebElement FiatDepositDailyLimits1 = driver.findElement(By.name("Fiat deposit daily limits"));
//	    FiatDepositDailyLimits1.clear();
//	    FiatDepositDailyLimits1.clear();
//	    FiatDepositDailyLimits1.sendKeys("999");
//	    WebElement FiatDepositMonthlyLimits1 = driver.findElement(By.name("Fiat deposit monthly limits"));
//	    FiatDepositMonthlyLimits1.clear();
//	    FiatDepositMonthlyLimits1.clear();
//	    FiatDepositMonthlyLimits1.sendKeys("29970");
//	    WebElement FiatDepositYearlyLimits1 = driver.findElement(By.name("Fiat deposit yearly limits"));
//	    FiatDepositYearlyLimits1.clear();
//	    FiatDepositYearlyLimits1.clear();
//	    FiatDepositYearlyLimits1.sendKeys("359639");
//	    WebElement FiatWithdrawalsDailyLimits1 = driver.findElement(By.name("Fiat withdrawals daily limits"));
//	    FiatWithdrawalsDailyLimits1.clear();
//	    FiatWithdrawalsDailyLimits1.clear();
//	    FiatWithdrawalsDailyLimits1.sendKeys("998");
//	    WebElement FiatWithdrawalsMonthlyLimits1 = driver.findElement(By.name("Fiat withdrawals monthly limits"));
//	    FiatWithdrawalsMonthlyLimits1.clear();
//	    FiatWithdrawalsMonthlyLimits1.clear();
//	    FiatWithdrawalsMonthlyLimits1.sendKeys("29969");
//	    WebElement FiatWithdrawalsYearlyLimits1 = driver.findElement(By.name("Fiat withdrawals yearly limits"));
//	    FiatWithdrawalsYearlyLimits1.clear();
//	    FiatWithdrawalsYearlyLimits1.clear();
//	    FiatWithdrawalsYearlyLimits1.sendKeys("359639");
//	    driver.findElement(By.name("Update button")).click();
//	    try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//	    driver.findElement(By.cssSelector(".btn-ok")).click();
//	  }
//	 
//	  public void addFeeCategory(String testName, String testDescription) {
//	    driver.findElement(By.linkText("account_balanceBank Details")).click();
//	    driver.findElement(By.linkText("Fee Categories")).click();
//	    driver.findElement(By.name("Modal button")).click();
//	    driver.findElement(By.name("Fee category name")).sendKeys(testName);
//	    driver.findElement(By.name("Fee category description")).sendKeys(testDescription);
//	    driver.findElement(By.name("Add button")).click();
//	  }
//	@Test
//	  public void addLegalstatus() {
//	    driver.findElement(By.linkText("account_balanceBank Details")).click();
//	    driver.findElement(By.linkText("Legal Status")).click();
//	    driver.findElement(By.name("Modal button")).click();
//	    driver.findElement(By.name("Legal status name")).sendKeys("new testing");
//	    driver.findElement(By.name("Legal status description")).sendKeys("testing");
//	    driver.findElement(By.name("Add button")).click();
//	  }
	@Test
	public void addBank() {
		driver.findElement(By.cssSelector("li:nth-child(7) span")).click();
	    driver.findElement(By.linkText("Add")).click();
	    {
	      WebElement dropdown = driver.findElement(By.id("currency"));
	      dropdown.findElement(By.xpath("//option[. = 'PAKISTANI RUPEES (PKR)']")).click();
	    }
	    {
	      WebElement element = driver.findElement(By.id("currency"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(element).clickAndHold().perform();
	    }
	    {
	      WebElement element = driver.findElement(By.id("currency"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(element).perform();
	    }
	    {
	      WebElement element = driver.findElement(By.id("currency"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(element).release().perform();
	    }
	    driver.findElement(By.id("currency")).click();
	    driver.findElement(By.name("Bank Name")).sendKeys("Habib Banks");
	    driver.findElement(By.name("Bank Symbol")).sendKeys("HBL");
	    driver.findElement(By.name("Branch code")).sendKeys("23456");
	    driver.findElement(By.name("Bank First address")).sendKeys("Berlin");
	    driver.findElement(By.name("Bank Second address")).sendKeys("Kaiserdamm 2714057 Berlin");
	    driver.findElement(By.name("Zip")).sendKeys("10115");
	    driver.findElement(By.name("City")).sendKeys("Berlin");
	    driver.findElement(By.cssSelector("#Country .vs__search")).click();
	    driver.findElement(By.cssSelector("#Country .vs__search")).sendKeys("Germany");
	    driver.findElement(By.cssSelector("#Country .vs__search")).sendKeys(Keys.ENTER);
	    driver.findElement(By.name("Phone")).sendKeys("923454445035");
	    driver.findElement(By.name("Swift")).sendKeys("BONUSTS45NX");
	    JavascriptExecutor js1 = (JavascriptExecutor) driver;
	    js1.executeScript("window.scrollBy(0,450)", "");
	    driver.findElement(By.name("Account title")).sendKeys("Danish Saber");
	    driver.findElement(By.name("Iban")).sendKeys("DE36SCBL000000112345");
	    driver.findElement(By.cssSelector("#Supported\\ Country .vs__search")).sendKeys("pak");
	    driver.findElement(By.cssSelector("#Supported\\ Country .vs__search")).sendKeys(Keys.ENTER);
	    driver.findElement(By.cssSelector("#Supported\\ Country .vs__search")).sendKeys("Germany");
	    driver.findElement(By.cssSelector("#Supported\\ Country .vs__search")).sendKeys(Keys.ENTER);
	    driver.findElement(By.cssSelector(".col-sm-3:nth-child(3) .custom-control-label")).click();
	    driver.findElement(By.name("1 input deposit static fee")).sendKeys("1");
	    driver.findElement(By.name("2 input deposit static fee")).sendKeys("1");
	    driver.findElement(By.name("3 input deposit static fee")).sendKeys("1");
	    JavascriptExecutor js2 = (JavascriptExecutor) driver;
	    js2.executeScript("window.scrollBy(0,450)", "");
	    driver.findElement(By.name("1 input withdraw static fee")).sendKeys("1");
	    driver.findElement(By.name("2 input withdraw static fee")).sendKeys("1");
	    driver.findElement(By.name("3 input withdraw static fee")).sendKeys("1");
	    driver.findElement(By.name("Add button")).click();
	}
//	@Test
//	public void addFiatCurr() {
//		System.out.println("haider");
//		driver.findElement(By.cssSelector("li:nth-child(6) span")).click();
//	    driver.findElement(By.linkText("Add Fiat")).click();
//	    driver.findElement(By.name("Symbol")).sendKeys("PKK");
//	    driver.findElement(By.name("Name")).sendKeys("Pakistani Rupees");
//	    driver.findElement(By.name("Priority")).sendKeys("2");
//	    driver.findElement(By.name("Min withdrawal")).sendKeys("10");
//	    driver.findElement(By.name("Daily withdrawal")).sendKeys("1000");
//	    driver.findElement(By.name("Revenue Account")).sendKeys("PK3520211109000001234567");
//	    driver.findElement(By.name("Bank name")).sendKeys("Allied Bank");
//	    driver.findElement(By.name("Bank symbol")).sendKeys("ABL");
//	    driver.findElement(By.name("Branch code")).sendKeys("2294");
//	    driver.findElement(By.name("Swift")).sendKeys("BOFAUS3NXXX");
//	    JavascriptExecutor js = (JavascriptExecutor) driver;
//	    js.executeScript("window.scrollBy(0,450)", "");
//	    driver.findElement(By.name("First address")).sendKeys("Islamabad");
//	    driver.findElement(By.name("Second address")).sendKeys("Islamabad capital territory");
//	    driver.findElement(By.name("zip")).sendKeys("44000");
//	    driver.findElement(By.name("City")).sendKeys("Islamabad capital territory");
//	    driver.findElement(By.name("Phone")).sendKeys("923456994485");
//	    driver.findElement(By.xpath("//*[@id=\"Country\"]/div/div[1]/input")).sendKeys("Pakistan");
//	    driver.findElement(By.xpath("//*[@id=\"Country\"]/div/div[1]/input")).sendKeys(Keys.ENTER);
//	    driver.findElement(By.name("Account title")).sendKeys("Max Mustermann");
//	    driver.findElement(By.name("Iban")).sendKeys("PK36SCBL0000001123456792");
//	    JavascriptExecutor js1 = (JavascriptExecutor) driver;
//	    js1.executeScript("window.scrollBy(0,450)", "");
//	    driver.findElement(By.xpath("//*[@id=\"Supported Countries\"]/div/div[1]/input")).sendKeys("Pakistan");
//	    driver.findElement(By.xpath("//*[@id=\"Supported Countries\"]/div/div[1]/input")).sendKeys(Keys.ENTER);
//	    driver.findElement(By.xpath("//*[@id=\"Supported Countries\"]/div/div[1]/input")).sendKeys("Ger");
//	    driver.findElement(By.xpath("//*[@id=\"Supported Countries\"]/div/div[1]/input")).sendKeys(Keys.ENTER);
//	    driver.findElement(By.xpath("//*[@id=\"Supported Countries\"]/div/div[1]/input")).sendKeys("United");
//	    driver.findElement(By.xpath("//*[@id=\"Supported Countries\"]/div/div[1]/input")).sendKeys(Keys.ENTER);
//	    try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		 driver.findElement(By.cssSelector(".col-sm-3:nth-child(3) .custom-control-label")).click();
//		    try {
//				Thread.sleep(2000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
////		    driver.findElement(By.name("1 input deposit percentage fee")).sendKeys("1");
////		    driver.findElement(By.name("1 input deposit minimum fee")).sendKeys("1");
////		    driver.findElement(By.name("1 input deposit maximum fee")).sendKeys("1");
////		    driver.findElement(By.name("2 input deposit percentage fee")).sendKeys("1");
////		    driver.findElement(By.name("2 input deposit minimum fee")).sendKeys("1");
////		    driver.findElement(By.name("2 input deposit maximum fee")).sendKeys("1");
////		    driver.findElement(By.name("3 input deposit percentage fee")).sendKeys("1");
////		    driver.findElement(By.name("3 input deposit minimum fee")).sendKeys("1");
////		    driver.findElement(By.name("3 input deposit maximum fee")).sendKeys("1");
////		    driver.findElement(By.name("4 input deposit percentage fee")).sendKeys("1");
////		    driver.findElement(By.name("4 input deposit minimum fee")).sendKeys("1");
////		    driver.findElement(By.name("1 input withdraw percentage fee")).sendKeys("1");
////		    driver.findElement(By.name("4 input deposit maximum fee")).sendKeys("1");
////		    driver.findElement(By.name("1 input withdraw percentage fee")).sendKeys("1");
////		    driver.findElement(By.name("1 input withdraw minimum fee")).sendKeys("1");
////		    driver.findElement(By.name("1 input withdraw maximum fee")).sendKeys("1");
////		    driver.findElement(By.name("2 input withdraw percentage fee")).sendKeys("1");
////		    driver.findElement(By.name("2 input withdraw minimum fee")).sendKeys("1");
////		    driver.findElement(By.name("2 input withdraw maximum fee")).sendKeys("1");
////		    driver.findElement(By.name("3 input withdraw percentage fee")).sendKeys("1");
////		    driver.findElement(By.name("3 input withdraw minimum fee")).sendKeys("1");
////		    driver.findElement(By.name("3 input withdraw maximum fee")).sendKeys("1");
////		    driver.findElement(By.name("4 input withdraw percentage fee")).sendKeys("1");
////		    driver.findElement(By.name("4 input withdraw minimum fee")).sendKeys("1");
////		    driver.findElement(By.name("4 input withdraw maximum fee")).sendKeys("1");
//		    driver.findElement(By.cssSelector(".left > .FeeAccounts:nth-child(2) > .custom-control:nth-child(2) > .custom-control-label")).click();
//		    driver.findElement(By.name("1 input deposit static fee")).sendKeys("1");
//		    driver.findElement(By.cssSelector(".right > .FeeAccounts:nth-child(2) > .custom-control:nth-child(2) > .custom-control-label")).click();
//		    driver.findElement(By.name("1 input withdraw static fee")).sendKeys("1");
//		    driver.findElement(By.name("Add button")).click();
//	 }
//	@Test
//	public void addMarket() {
//		 driver.findElement(By.cssSelector("li:nth-child(5) span:nth-child(2)")).click();	
//		 try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		    driver.findElement(By.linkText("Create")).click();
//		    {
//		      WebElement dropdown = driver.findElement(By.id("base"));
////		      dropdown.findElement(By.xpath("//option[. = 'ETHEREUM (ETH)']")).click();
//		      dropdown.findElement(By.xpath("//option[. = 'BLOCK360 (BOK)']")).click();
//		      
//		    }
//		    try {
//				Thread.sleep(2000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//		    Select quoteCurr = new Select(driver.findElement(By.id("quote")));
//			quoteCurr.selectByIndex(4);
//			
//			 driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/div[2]/div/div/div/div[2]/div[2]/div/div[2]/input[1]")).sendKeys("1");
//			 driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/div[2]/div/div/div/div[2]/div[2]/div/div[3]/input[1]")).sendKeys("1");
//		    driver.findElement(By.name("min_base")).sendKeys("0.1");
//		    driver.findElement(By.name("max_base")).sendKeys("10");
//		    driver.findElement(By.name("min_quote")).sendKeys("0.0001");
//		    driver.findElement(By.name("max_quote")).sendKeys("1");
//		    driver.findElement(By.name("tick")).sendKeys("0.00001");
//		    driver.findElement(By.name("priority")).sendKeys("1");
//		    driver.findElement(By.name("Next")).click();
//		    driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) > .custom-control > .custom-control-label")).click();
//		    driver.findElement(By.name("buyer_fee_base")).sendKeys("10");
//		    driver.findElement(By.name("min_buy_base_fee")).sendKeys("0.01");
//		    driver.findElement(By.name("max_buy_base_fee")).sendKeys("0.5");
//		    driver.findElement(By.name("seller_fee_base")).sendKeys("10");
//		    driver.findElement(By.name("min_sell_base_fee")).sendKeys("0.01");
//		    driver.findElement(By.name("max_sell_base_fee")).sendKeys("0.5");
//		    driver.findElement(By.cssSelector(".col-sm-6:nth-child(3) > .custom-control > .custom-control-label")).click();
//		    driver.findElement(By.name("buyer_fee_quote")).sendKeys("5");
//		    driver.findElement(By.name("min_buy_quote_fee")).sendKeys("0.000001");
//		    driver.findElement(By.name("max_buy_quote_fee")).sendKeys("0.00001");
//		    driver.findElement(By.name("seller_fee_quote")).sendKeys("5");
//		    driver.findElement(By.name("min_sell_quote_fee")).sendKeys("0.000001");
//		    driver.findElement(By.name("max_sell_quote_fee")).sendKeys("0.00001");
//		    driver.findElement(By.cssSelector(".sameAsAbove:nth-child(1) > .custom-control-label")).click();
//		    driver.findElement(By.name("mkr_tkr_dis")).sendKeys("15");
//		    driver.findElement(By.name("Update")).click();
//	}
//	@Test
//	public void addToken() {
//		driver.findElement(By.cssSelector("li:nth-child(6) span")).click();
//	    driver.findElement(By.linkText("Add Token")).click();
//	    driver.findElement(By.id("symbol")).sendKeys("BOK");
//	    driver.findElement(By.id("name")).sendKeys("Block360");
//	    driver.findElement(By.id("decimals")).sendKeys("8");
//	    {
//	      WebElement dropdown = driver.findElement(By.id("Token chain"));
//	      dropdown.findElement(By.xpath("//option[. = 'ETHEREUM (ETH)']")).click();
//	    }
//	    {
//	      WebElement element = driver.findElement(By.id("Token chain"));
//	      Actions builder = new Actions(driver);
//	      builder.moveToElement(element).clickAndHold().perform();
//	    }
//	    {
//	      WebElement element = driver.findElement(By.id("Token chain"));
//	      Actions builder = new Actions(driver);
//	      builder.moveToElement(element).perform();
//	    }
//	    {
//	      WebElement element = driver.findElement(By.id("Token chain"));
//	      Actions builder = new Actions(driver);
//	      builder.moveToElement(element).release().perform();
//	    }
//	    driver.findElement(By.id("Token chain")).click();
//	    driver.findElement(By.id("minwithdraw")).sendKeys("0xCf78bBf76545B7134FcceD0D11b69c17e2923b5c");
//	    driver.findElement(By.name("Token Priority")).sendKeys("1");
//	    driver.findElement(By.name("Min Threshold")).sendKeys("10");
//	    driver.findElement(By.name("Opt threshold")).sendKeys("100");
//	    JavascriptExecutor js2 = (JavascriptExecutor) driver;
//	    js2.executeScript("window.scrollBy(0,450)", "");
//	    driver.findElement(By.name("Exe Threshold")).sendKeys("110");
//	    driver.findElement(By.name("Network Fee")).sendKeys("0.1");
//	    driver.findElement(By.name("Min withdrawal")).sendKeys("1");
//	    driver.findElement(By.name("Fee Account")).sendKeys("0xCf78bBf76545B7134FcceD0D11b69c17e2923b5c");
//	    driver.findElement(By.name("Opt ercentage")).sendKeys("10");
//	    driver.findElement(By.name("Gas Limit")).sendKeys("21000");
//	    driver.findElement(By.name("Daily withdrawal")).sendKeys("10");
//	    driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/form/div[3]/div/button[1]")).click();
//	}
	
//	@AfterTest
//	void closeDriver() {
//		driver.quit();
//	}

}
