package restAssuredApiAutomation;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class chromeTest {
	
	WebDriver driver;
	
	@BeforeTest
	public void callDriver() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.get("https://trader.block360.io/");	
	}
	
	@BeforeMethod
	public void setup(){
		driver.findElement(By.linkText("Log in")).click();
		driver.findElement(By.id("username")).sendKeys("haideryaqoobengr@gmail.com");
		driver.findElement(By.id("password")).sendKeys("bapa2526");
	    driver.findElement(By.id("kc-login")).click();
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[1]/nav/ul/li[4]/a")).click();
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    driver.findElement(By.cssSelector(".col-3 li:nth-child(3)")).click();
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    String balance = driver.findElement(By.xpath("//*[@id=\"id-wallet\"]/div/div[1]/ul/li[2]/span")).getText();
	    System.out.println("balance before withdraw is:"+balance);
	    driver.findElement(By.cssSelector(".wallet-tab li:nth-child(2) > a")).click();
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    
	    driver.findElement(By.xpath("//*[@id=\"id-wallet\"]/div/div[2]/div[2]/div/div[1]/div/input ")).sendKeys("0xbF444b461bAaC0b2c0645B6f856186351792EC9C");
	    driver.findElement(By.xpath("//*[@id=\"id-wallet\"]/div/div[2]/div[2]/div/div[2]/div[1]/div/input")).sendKeys("10");
	    driver.findElement(By.xpath("//*[@id=\"id-wallet\"]/div/div[2]/div[2]/div/div[2]/div[2]/div/textarea")).sendKeys("testing");
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    driver.findElement(By.xpath("//*[@id=\"id-wallet\"]/div/div[2]/div[2]/div/div[3]/button")).click();
	    try {
				Thread.sleep(15000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    driver.findElement(By.cssSelector(".col-3 li:nth-child(3)")).click();
	    String newbalance = driver.findElement(By.xpath("//*[@id=\"id-wallet\"]/div/div[1]/ul/li[2]/span")).getText();
	    System.out.println("new balance is : "+ newbalance);
	    
	    newbalance = newbalance.replace(",","");
	    newbalance = newbalance.indexOf(".") < 0 ? newbalance : newbalance.replaceAll("0*$", "").replaceAll("\\.$", "");
	    int j=Integer.parseInt(newbalance);  
	    String newBalance = Integer.toString(j);
	    
	    balance = balance.replace(",","");
	    balance = balance.indexOf(".") < 0 ? balance : balance.replaceAll("0*$", "").replaceAll("\\.$", "");
	    int i=Integer.parseInt(balance);  
	    int forAssert = i - 10;
	    String BallanceBeforeWithdraw = Integer.toString(forAssert);
//	    balance = balance.replace(".","");

//	    String s = new DecimalFormat("0.####").format(Double.parseDouble(balance));
//	    System.out.println(s);
//	    
//	    
	    System.out.println("balance after comma is : "+ balance);
	    Assert.assertEquals(newBalance, BallanceBeforeWithdraw);


	}
	
	@Test
	 void getweatherDetails()
	 {
	  //Specify base URI
	  RestAssured.baseURI="http://restapi.demoqa.com/utilities/weather/city";
	  
	  //Request object
	  RequestSpecification httpRequest=RestAssured.given();
	  
	  //Response object
	  Response response=httpRequest.request(Method.GET,"/Hyderabad");
	  
	  //print response in console window
	  
	  String responseBody=response.getBody().asString();
	  System.out.println("Response Body is:" +responseBody);
	  
	  //status code validation
	  int statusCode=response.getStatusCode();
	  System.out.println("Status code is: "+statusCode);
	  Assert.assertEquals(statusCode, 200);
	  
	  //status line verification
	  String statusLine=response.getStatusLine();
	  System.out.println("Status line is:"+statusLine);
	  Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
	  
	 }
	
	@AfterMethod
	void closeDriver() {
		driver.quit();
	}

}
