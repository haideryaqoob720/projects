// var Tx = require('ethereumjs-tx')
const Tx = require('ethereumjs-tx').Transaction
var Web3 = require('web3')
const web3 = new Web3('https://rinkeby.infura.io/v3/ccf9448be3fe40ed82aab9ffd81ae7f2');
// const web3 = new Web3('https://ropsten.infura.io/v3/ccf9448be3fe40ed82aab9ffd81ae7f2');

// a1 = web3.eth.accounts.create();
// a2 = web3.eth.accounts.create();

// account1 = a1.privateKey;
// account2 = a2.privateKey;
// const account1 = '0x0424Cf3e9437663E05B07Da13b0272D05fBA5b7E';
const account1 = '0x0424Cf3e9437663E05B07Da13b0272D05fBA5b7E';

const account2 = '0xA43Fe3239C44384eb77C0361e67dA964f2096333';

web3.eth.getBalance(account1, (err, bal) => {
    console.log('account 1 balance: ', web3.utils.fromWei(bal, 'ether'))
})
web3.eth.getBalance(account2, (err, bal) => {
    console.log('account 2 balance: ', web3.utils.fromWei(bal, 'ether'))
})

const privateKey1 = Buffer.from('CFBEC5351E2793E60421EA6C21F1C9CF4AA7D36C2A9DEE01E5B7FBAEF7121D4F', 'hex')
// const privateKey = Buffer.from(
//     'e331b6d69882b4cb4ea581d88e0b604039a3de5967688d3dcffdd2270c0fd109',
//     'hex',
//   )

web3.eth.getTransactionCount(account1, (err, txCount) => {
    //build the transaction
    const txObject = {
        nonce: web3.utils.toHex(txCount),
        to: account2,
        from:'0x0424Cf3e9437663E05B07Da13b0272D05fBA5b7E',
        value: web3.utils.toHex(web3.utils.toWei('1', 'ether')),
        gasLimit: web3.utils.toHex(21000),
        gasPrice: Web3.utils.toHex(web3.utils.toWei('10', 'gwei'))
    }
    console.log('object:', txObject);
    // sign transaction

    const tx = new Tx(txObject, { chain: 'rinkeby' })
    console.log('signed privateKey1: ', tx.sign(privateKey1))

    const serializedTransaction = tx.serialize()
    const raw = '0x' + serializedTransaction.toString('hex')

    //Broadcast the transaction
    web3.eth.sendSignedTransaction(raw, (err, txHash) => {
        console.log('txHash: ', txHash)
    })
})



// console.log('account1: ', account1)
// console.log('account2: ', account2)

// newAccount = web3.eth.accounts.create();// creating new account

// //encrypting private key and put password
// encyprivKey = web3.eth.accounts.encrypt(newAccount.privateKey, 'passwordhaider');

// //decrypting again getting private key
// decryKey = web3.eth.accounts.decrypt(encyprivKey, 'passwordhaider');
// console.log('private key is : ', decryKey);

// newWallet = web3.eth.accounts.wallet.create(10);
// console.log('newWallet  is : ', newWallet);

// //encrypting wallet
// encyprivKey = web3.eth.accounts.wallet.encrypt('passwordhaider');
// console.log('encrypted wallet is : ', encyprivKey)