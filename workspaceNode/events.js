var events = require('events');
var eventEmitterObj = new events.EventEmitter();

//Create an event handler:
var myEventHandler = function () {
  console.log('I hear a scream!');
}

//Assign the event handler to an event:
eventEmitterObj.on('nameOfEventHandler', myEventHandler);

//Fire the 'scream' event:
eventEmitterObj.emit('nameOfEventHandler');