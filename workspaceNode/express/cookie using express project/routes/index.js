var express = require('express');
var router = express.Router();



//basic route for homepage 
router.get('/', (req, res)=>{ 
res.send('welcome to express app'); 
}); 

//JSON object to be added to cookie 
let users = { 
name : "Ritik", 
Age : "18"
} 

//Route for adding cookie 
router.get('/setuser', (req, res)=>{ 
res.cookie("userData", users); 
console.log("this is cookie:", res.cookie);
res.send('user data added to cookie'); 
}); 

router.get('/getuser', (req, res)=>{ 
//shows all the cookies 
res.send(req.cookies); 
}); 

//Route for destroying cookie 
router.get('/logout', (req, res)=>{ 
//it will clear the userData cookie 
res.clearCookie('userData'); 
res.send('user logout successfully'); 
}); 


module.exports = router;
