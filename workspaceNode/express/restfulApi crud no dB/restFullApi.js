var Joi = require('joi')
var express = require('express');
app = express();

app.use(express.json());

var myArr =
    [
        { id: 1, name: 'first' },
        { id: 2, name: 'two' },
        { id: 3, name: 'three' }
    ]
var test = { abc: 123 }

app.get('/', (req, res, next) => {
    console.log('first')
    next();                          //using middle ware
});

app.get('/', (req, res) => {
    console.log("haider");
    res.send(myArr);
});
app.get('/students/api/', (req, res) => {
    res.send(myArr);
})

app.get('/students/api/:id', (req, res) => {
    const std = myArr.find(element => element.id === parseInt(req.params.id));
    if (!std) return res.status('404').send('student with given id not found')
    res.send(std);
})

app.post('/students/api/', (req, res) => {
    const chkValid = validatInput(req.body)
    if (chkValid.error) {
        res.status(400).send(chkValid.error.details[0].message);
        return;
    }

    const std = {
        id: myArr.length + 1,
        name: req.body.name
    };
    myArr.push(std);
    res.send(std);
});

app.put('/students/api/:id', (req, res) => {
    const std = myArr.find(element => element.id === parseInt(req.params.id));
    if (!std) {
        return res.status(400).send("student with this id doesn't exist")
    }
    const { error } = validatInput(req.body)
    if (error) {
        res.status(400).send(error.details[0].message);
        return;
    }
    std.name = req.body.name;
    res.send(std);
})

app.delete('/students/api/:id', (req, res) => {
    const std = myArr.find(element => element.id === parseInt(req.params.id));
    if (!std) {
        return res.status(400).send("student with this id doesn't exist")
    }
    const index = myArr.indexOf(std);
    myArr.splice(index, 1); 
    res.send(std);
});

function validatInput(input) {
    const schema = {
        name: Joi.string().min(3).required()
    };

    return result = Joi.validate(input, schema);
}

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on ${port}...`));