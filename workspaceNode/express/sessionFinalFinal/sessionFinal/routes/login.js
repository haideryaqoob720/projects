var express = require('express');
var router = express.Router();

router.all('/loginStart', function(req, res, next) {
//   res.render('loginStarted', { title: 'Express' });
var email = req.body.email;
var password = req.body.password;
req.session.email = email;
req.session.password = password;
  if(req.session.email && req.session.password) {
    res.write('<h1>Hello '+req.session.email+'</h1>');
    res.write('<a href="/logout">Logout</a>');
    res.end();
    // res.render('loginStarted', { title: 'Express' });
} else {
    res.write('<h1>Please login first.</h1>');
    res.write('<a href="/">Login</a>');
    res.end();
}
});
router.get('/logout', function(req, res, next) {
    //res.render('loginForm', { title: 'Express' });
    // if the user logs out, destroy all of their individual session
	// information
	req.session.destroy(function(err) {
		if(err) {
			console.log(err);
		} else {
			res.redirect('/');
		}
    });
    // req.session.destroy();
    // return res.status(200).send();
    
  });
module.exports = router;