var http = require('http');
var fs = require('fs');
var url = require('url');
var upperCase = require('upper-case');

http.createServer(function (req, res){
    var q = url.parse(req.url, true);
    var fileName = '.' + q.pathname;
    console.log(upperCase(fileName));
    fs.readFile(fileName, function(err, data){
        if(err){
            res.writeHead(404, {'Content-Type' : 'text/html'});
            return res.end('404 not found');
        }
        res.writeHead(200, {'Content-Type' : 'text/html'});
        res.write(data);
        return res.end();
    })
}).listen(8080);