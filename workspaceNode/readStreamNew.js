var fs = require('fs');

var readStream = fs.createReadStream('./demofile.txt');

/*Write to the console when the file is opened:*/
readStream.on('open', function () {
  console.log('The file is open');
  //Fire the 'scream' event:
eventEmitterObj.emit('nameOfEventHandler');
});

var events = require('events');
var eventEmitterObj = new events.EventEmitter();

//Create an event handler:
var myEventHandler = function () {
  console.log('I hear a scream! and event listner is working fine');
}

//Assign the event handler to an event:
eventEmitterObj.on('nameOfEventHandler', myEventHandler);

